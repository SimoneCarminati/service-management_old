﻿namespace Service_Management
{
    partial class Form_Riparazione
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form_Riparazione));
            this.tb_NomeCliente = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btn_OK = new System.Windows.Forms.Button();
            this.imageList = new System.Windows.Forms.ImageList(this.components);
            this.btn_Cancel = new System.Windows.Forms.Button();
            this.tabControl = new System.Windows.Forms.TabControl();
            this.tabCliente = new System.Windows.Forms.TabPage();
            this.btn_cercaCliente = new System.Windows.Forms.Button();
            this.imageList_Tiny = new System.Windows.Forms.ImageList(this.components);
            this.lbl_dataDocReso = new System.Windows.Forms.Label();
            this.dtp_dataDocReso = new System.Windows.Forms.DateTimePicker();
            this.dtp_dataOrdRip = new System.Windows.Forms.DateTimePicker();
            this.lbl_dataOrdRip = new System.Windows.Forms.Label();
            this.tb_numScarCLI = new System.Windows.Forms.TextBox();
            this.lbl_numSCARcli = new System.Windows.Forms.Label();
            this.lbl_ordRip = new System.Windows.Forms.Label();
            this.tb_ordRiparaz = new System.Windows.Forms.TextBox();
            this.tb_difettoSegnalatoDalCliente = new System.Windows.Forms.TextBox();
            this.tb_docReso = new System.Windows.Forms.TextBox();
            this.tb_scarCliente = new System.Windows.Forms.TextBox();
            this.lbl_docReso = new System.Windows.Forms.Label();
            this.lbl_SCARcli = new System.Windows.Forms.Label();
            this.lbl_difRiscCli = new System.Windows.Forms.Label();
            this.tabArticolo = new System.Windows.Forms.TabPage();
            this.tb_LottoArticolo = new System.Windows.Forms.TextBox();
            this.btn_cercaArticolo = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.tb_VersioneArticolo = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.tb_SerialeArticolo = new System.Windows.Forms.TextBox();
            this.tb_NoteArticolo = new System.Windows.Forms.TextBox();
            this.tb_CodiceArticoloCliente = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.tb_Articolo = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.tabRiparazione = new System.Windows.Forms.TabPage();
            this.label17 = new System.Windows.Forms.Label();
            this.dtp_DataRiparazione = new System.Windows.Forms.DateTimePicker();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.cmb_statoRiparazione = new System.Windows.Forms.ComboBox();
            this.tb_noteRiparazione = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.cmb_CausaDifetto = new System.Windows.Forms.ComboBox();
            this.cmb_tipoRiparazione = new System.Windows.Forms.ComboBox();
            this.tb_dettaglioRiparazione = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.tb_difettoRiscontrato = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.tb_costoPreventivato = new System.Windows.Forms.MaskedTextBox();
            this.tb_costoSostenuto = new System.Windows.Forms.MaskedTextBox();
            this.tb_costoAddebitato = new System.Windows.Forms.MaskedTextBox();
            this.tabControl.SuspendLayout();
            this.tabCliente.SuspendLayout();
            this.tabArticolo.SuspendLayout();
            this.tabRiparazione.SuspendLayout();
            this.SuspendLayout();
            // 
            // tb_NomeCliente
            // 
            this.tb_NomeCliente.BackColor = System.Drawing.Color.Salmon;
            this.tb_NomeCliente.Location = new System.Drawing.Point(147, 12);
            this.tb_NomeCliente.Name = "tb_NomeCliente";
            this.tb_NomeCliente.ReadOnly = true;
            this.tb_NomeCliente.Size = new System.Drawing.Size(552, 20);
            this.tb_NomeCliente.TabIndex = 1;
            this.tb_NomeCliente.TextChanged += new System.EventHandler(this.tb_Nome_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(16, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(39, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Cliente";
            // 
            // btn_OK
            // 
            this.btn_OK.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btn_OK.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btn_OK.ImageIndex = 3;
            this.btn_OK.ImageList = this.imageList;
            this.btn_OK.Location = new System.Drawing.Point(308, 350);
            this.btn_OK.Name = "btn_OK";
            this.btn_OK.Size = new System.Drawing.Size(47, 47);
            this.btn_OK.TabIndex = 100;
            this.btn_OK.UseVisualStyleBackColor = true;
            this.btn_OK.Click += new System.EventHandler(this.btn_OK_Click);
            // 
            // imageList
            // 
            this.imageList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList.ImageStream")));
            this.imageList.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList.Images.SetKeyName(0, "client.png");
            this.imageList.Images.SetKeyName(1, "boards.png");
            this.imageList.Images.SetKeyName(2, "repairs.png");
            this.imageList.Images.SetKeyName(3, "ok.png");
            this.imageList.Images.SetKeyName(4, "cancel.png");
            this.imageList.Images.SetKeyName(5, "search_32x32.png");
            this.imageList.Images.SetKeyName(6, "search_24x24.png");
            // 
            // btn_Cancel
            // 
            this.btn_Cancel.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btn_Cancel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btn_Cancel.ImageIndex = 4;
            this.btn_Cancel.ImageList = this.imageList;
            this.btn_Cancel.Location = new System.Drawing.Point(415, 350);
            this.btn_Cancel.Name = "btn_Cancel";
            this.btn_Cancel.Size = new System.Drawing.Size(47, 47);
            this.btn_Cancel.TabIndex = 101;
            this.btn_Cancel.UseVisualStyleBackColor = true;
            this.btn_Cancel.Click += new System.EventHandler(this.btn_Cancel_Click);
            // 
            // tabControl
            // 
            this.tabControl.Appearance = System.Windows.Forms.TabAppearance.Buttons;
            this.tabControl.Controls.Add(this.tabCliente);
            this.tabControl.Controls.Add(this.tabArticolo);
            this.tabControl.Controls.Add(this.tabRiparazione);
            this.tabControl.ImageList = this.imageList;
            this.tabControl.Location = new System.Drawing.Point(8, 8);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.ShowToolTips = true;
            this.tabControl.Size = new System.Drawing.Size(785, 335);
            this.tabControl.TabIndex = 15;
            // 
            // tabCliente
            // 
            this.tabCliente.Controls.Add(this.btn_cercaCliente);
            this.tabCliente.Controls.Add(this.lbl_dataDocReso);
            this.tabCliente.Controls.Add(this.dtp_dataDocReso);
            this.tabCliente.Controls.Add(this.dtp_dataOrdRip);
            this.tabCliente.Controls.Add(this.lbl_dataOrdRip);
            this.tabCliente.Controls.Add(this.tb_numScarCLI);
            this.tabCliente.Controls.Add(this.lbl_numSCARcli);
            this.tabCliente.Controls.Add(this.lbl_ordRip);
            this.tabCliente.Controls.Add(this.tb_ordRiparaz);
            this.tabCliente.Controls.Add(this.tb_difettoSegnalatoDalCliente);
            this.tabCliente.Controls.Add(this.tb_docReso);
            this.tabCliente.Controls.Add(this.tb_scarCliente);
            this.tabCliente.Controls.Add(this.lbl_docReso);
            this.tabCliente.Controls.Add(this.lbl_SCARcli);
            this.tabCliente.Controls.Add(this.lbl_difRiscCli);
            this.tabCliente.Controls.Add(this.tb_NomeCliente);
            this.tabCliente.Controls.Add(this.label1);
            this.tabCliente.ImageIndex = 0;
            this.tabCliente.Location = new System.Drawing.Point(4, 47);
            this.tabCliente.Name = "tabCliente";
            this.tabCliente.Padding = new System.Windows.Forms.Padding(3);
            this.tabCliente.Size = new System.Drawing.Size(777, 284);
            this.tabCliente.TabIndex = 0;
            this.tabCliente.ToolTipText = "Dettagli Cliente";
            this.tabCliente.UseVisualStyleBackColor = true;
            // 
            // btn_cercaCliente
            // 
            this.btn_cercaCliente.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btn_cercaCliente.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btn_cercaCliente.ImageIndex = 0;
            this.btn_cercaCliente.ImageList = this.imageList_Tiny;
            this.btn_cercaCliente.Location = new System.Drawing.Point(728, 6);
            this.btn_cercaCliente.Name = "btn_cercaCliente";
            this.btn_cercaCliente.Size = new System.Drawing.Size(30, 30);
            this.btn_cercaCliente.TabIndex = 2;
            this.btn_cercaCliente.UseVisualStyleBackColor = true;
            this.btn_cercaCliente.Click += new System.EventHandler(this.btn_cercaCliente_Click);
            // 
            // imageList_Tiny
            // 
            this.imageList_Tiny.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList_Tiny.ImageStream")));
            this.imageList_Tiny.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList_Tiny.Images.SetKeyName(0, "search.png");
            // 
            // lbl_dataDocReso
            // 
            this.lbl_dataDocReso.AutoSize = true;
            this.lbl_dataDocReso.Location = new System.Drawing.Point(401, 142);
            this.lbl_dataDocReso.Name = "lbl_dataDocReso";
            this.lbl_dataDocReso.Size = new System.Drawing.Size(127, 13);
            this.lbl_dataDocReso.TabIndex = 38;
            this.lbl_dataDocReso.Text = "Data Documento di Reso";
            // 
            // dtp_dataDocReso
            // 
            this.dtp_dataDocReso.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtp_dataDocReso.Location = new System.Drawing.Point(558, 139);
            this.dtp_dataDocReso.Name = "dtp_dataDocReso";
            this.dtp_dataDocReso.Size = new System.Drawing.Size(200, 20);
            this.dtp_dataDocReso.TabIndex = 8;
            this.dtp_dataDocReso.Value = new System.DateTime(2019, 1, 20, 11, 14, 2, 297);
            // 
            // dtp_dataOrdRip
            // 
            this.dtp_dataOrdRip.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtp_dataOrdRip.Location = new System.Drawing.Point(558, 98);
            this.dtp_dataOrdRip.Name = "dtp_dataOrdRip";
            this.dtp_dataOrdRip.Size = new System.Drawing.Size(200, 20);
            this.dtp_dataOrdRip.TabIndex = 6;
            this.dtp_dataOrdRip.Value = new System.DateTime(2019, 1, 20, 11, 14, 2, 300);
            // 
            // lbl_dataOrdRip
            // 
            this.lbl_dataOrdRip.AutoSize = true;
            this.lbl_dataOrdRip.Location = new System.Drawing.Point(401, 101);
            this.lbl_dataOrdRip.Name = "lbl_dataOrdRip";
            this.lbl_dataOrdRip.Size = new System.Drawing.Size(123, 13);
            this.lbl_dataOrdRip.TabIndex = 37;
            this.lbl_dataOrdRip.Text = "Data Ordine Riparazione";
            // 
            // tb_numScarCLI
            // 
            this.tb_numScarCLI.Location = new System.Drawing.Point(558, 55);
            this.tb_numScarCLI.Name = "tb_numScarCLI";
            this.tb_numScarCLI.Size = new System.Drawing.Size(200, 20);
            this.tb_numScarCLI.TabIndex = 4;
            this.tb_numScarCLI.Text = "N/A";
            // 
            // lbl_numSCARcli
            // 
            this.lbl_numSCARcli.AutoSize = true;
            this.lbl_numSCARcli.Location = new System.Drawing.Point(401, 59);
            this.lbl_numSCARcli.Name = "lbl_numSCARcli";
            this.lbl_numSCARcli.Size = new System.Drawing.Size(111, 13);
            this.lbl_numSCARcli.TabIndex = 36;
            this.lbl_numSCARcli.Text = "Numero SCAR Cliente";
            // 
            // lbl_ordRip
            // 
            this.lbl_ordRip.AutoSize = true;
            this.lbl_ordRip.Location = new System.Drawing.Point(17, 100);
            this.lbl_ordRip.Name = "lbl_ordRip";
            this.lbl_ordRip.Size = new System.Drawing.Size(97, 13);
            this.lbl_ordRip.TabIndex = 15;
            this.lbl_ordRip.Text = "Ordine Riparazione";
            // 
            // tb_ordRiparaz
            // 
            this.tb_ordRiparaz.Location = new System.Drawing.Point(147, 97);
            this.tb_ordRiparaz.Name = "tb_ordRiparaz";
            this.tb_ordRiparaz.Size = new System.Drawing.Size(200, 20);
            this.tb_ordRiparaz.TabIndex = 5;
            this.tb_ordRiparaz.Text = "N/A";
            // 
            // tb_difettoSegnalatoDalCliente
            // 
            this.tb_difettoSegnalatoDalCliente.Location = new System.Drawing.Point(147, 180);
            this.tb_difettoSegnalatoDalCliente.Multiline = true;
            this.tb_difettoSegnalatoDalCliente.Name = "tb_difettoSegnalatoDalCliente";
            this.tb_difettoSegnalatoDalCliente.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.tb_difettoSegnalatoDalCliente.Size = new System.Drawing.Size(611, 90);
            this.tb_difettoSegnalatoDalCliente.TabIndex = 9;
            this.tb_difettoSegnalatoDalCliente.Text = "N/A";
            // 
            // tb_docReso
            // 
            this.tb_docReso.Location = new System.Drawing.Point(147, 138);
            this.tb_docReso.Name = "tb_docReso";
            this.tb_docReso.Size = new System.Drawing.Size(200, 20);
            this.tb_docReso.TabIndex = 7;
            this.tb_docReso.Text = "N/A";
            // 
            // tb_scarCliente
            // 
            this.tb_scarCliente.Location = new System.Drawing.Point(147, 55);
            this.tb_scarCliente.Name = "tb_scarCliente";
            this.tb_scarCliente.Size = new System.Drawing.Size(200, 20);
            this.tb_scarCliente.TabIndex = 3;
            this.tb_scarCliente.Text = "N/A";
            // 
            // lbl_docReso
            // 
            this.lbl_docReso.AutoSize = true;
            this.lbl_docReso.Location = new System.Drawing.Point(17, 141);
            this.lbl_docReso.Name = "lbl_docReso";
            this.lbl_docReso.Size = new System.Drawing.Size(101, 13);
            this.lbl_docReso.TabIndex = 14;
            this.lbl_docReso.Text = "Documento di Reso";
            // 
            // lbl_SCARcli
            // 
            this.lbl_SCARcli.AutoSize = true;
            this.lbl_SCARcli.Location = new System.Drawing.Point(17, 58);
            this.lbl_SCARcli.Name = "lbl_SCARcli";
            this.lbl_SCARcli.Size = new System.Drawing.Size(71, 13);
            this.lbl_SCARcli.TabIndex = 13;
            this.lbl_SCARcli.Text = "SCAR Cliente";
            // 
            // lbl_difRiscCli
            // 
            this.lbl_difRiscCli.AutoSize = true;
            this.lbl_difRiscCli.Location = new System.Drawing.Point(17, 210);
            this.lbl_difRiscCli.Name = "lbl_difRiscCli";
            this.lbl_difRiscCli.Size = new System.Drawing.Size(92, 26);
            this.lbl_difRiscCli.TabIndex = 12;
            this.lbl_difRiscCli.Text = "Difetto Segnalato \r\nDal Cliente";
            // 
            // tabArticolo
            // 
            this.tabArticolo.Controls.Add(this.tb_LottoArticolo);
            this.tabArticolo.Controls.Add(this.btn_cercaArticolo);
            this.tabArticolo.Controls.Add(this.label3);
            this.tabArticolo.Controls.Add(this.tb_VersioneArticolo);
            this.tabArticolo.Controls.Add(this.label4);
            this.tabArticolo.Controls.Add(this.label5);
            this.tabArticolo.Controls.Add(this.tb_SerialeArticolo);
            this.tabArticolo.Controls.Add(this.tb_NoteArticolo);
            this.tabArticolo.Controls.Add(this.tb_CodiceArticoloCliente);
            this.tabArticolo.Controls.Add(this.label7);
            this.tabArticolo.Controls.Add(this.label8);
            this.tabArticolo.Controls.Add(this.tb_Articolo);
            this.tabArticolo.Controls.Add(this.label9);
            this.tabArticolo.ImageIndex = 1;
            this.tabArticolo.Location = new System.Drawing.Point(4, 47);
            this.tabArticolo.Name = "tabArticolo";
            this.tabArticolo.Padding = new System.Windows.Forms.Padding(3);
            this.tabArticolo.Size = new System.Drawing.Size(777, 284);
            this.tabArticolo.TabIndex = 1;
            this.tabArticolo.ToolTipText = "Dettagli Articolo";
            this.tabArticolo.UseVisualStyleBackColor = true;
            // 
            // tb_LottoArticolo
            // 
            this.tb_LottoArticolo.Location = new System.Drawing.Point(558, 98);
            this.tb_LottoArticolo.Name = "tb_LottoArticolo";
            this.tb_LottoArticolo.Size = new System.Drawing.Size(200, 20);
            this.tb_LottoArticolo.TabIndex = 6;
            this.tb_LottoArticolo.Text = "N/A";
            // 
            // btn_cercaArticolo
            // 
            this.btn_cercaArticolo.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btn_cercaArticolo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btn_cercaArticolo.ImageIndex = 0;
            this.btn_cercaArticolo.ImageList = this.imageList_Tiny;
            this.btn_cercaArticolo.Location = new System.Drawing.Point(728, 6);
            this.btn_cercaArticolo.Name = "btn_cercaArticolo";
            this.btn_cercaArticolo.Size = new System.Drawing.Size(30, 30);
            this.btn_cercaArticolo.TabIndex = 2;
            this.btn_cercaArticolo.UseVisualStyleBackColor = true;
            this.btn_cercaArticolo.Click += new System.EventHandler(this.btn_cercaArticolo_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(401, 101);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(69, 13);
            this.label3.TabIndex = 54;
            this.label3.Text = "Lotto Articolo";
            // 
            // tb_VersioneArticolo
            // 
            this.tb_VersioneArticolo.Location = new System.Drawing.Point(558, 55);
            this.tb_VersioneArticolo.Name = "tb_VersioneArticolo";
            this.tb_VersioneArticolo.Size = new System.Drawing.Size(200, 20);
            this.tb_VersioneArticolo.TabIndex = 4;
            this.tb_VersioneArticolo.Text = "N/A";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(401, 59);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(86, 13);
            this.label4.TabIndex = 53;
            this.label4.Text = "Versione Articolo";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(17, 100);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(77, 13);
            this.label5.TabIndex = 45;
            this.label5.Text = "Seriale Articolo";
            // 
            // tb_SerialeArticolo
            // 
            this.tb_SerialeArticolo.Location = new System.Drawing.Point(147, 97);
            this.tb_SerialeArticolo.Name = "tb_SerialeArticolo";
            this.tb_SerialeArticolo.Size = new System.Drawing.Size(200, 20);
            this.tb_SerialeArticolo.TabIndex = 5;
            this.tb_SerialeArticolo.Text = "N/A";
            // 
            // tb_NoteArticolo
            // 
            this.tb_NoteArticolo.Location = new System.Drawing.Point(147, 180);
            this.tb_NoteArticolo.Multiline = true;
            this.tb_NoteArticolo.Name = "tb_NoteArticolo";
            this.tb_NoteArticolo.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.tb_NoteArticolo.Size = new System.Drawing.Size(611, 90);
            this.tb_NoteArticolo.TabIndex = 7;
            this.tb_NoteArticolo.Text = "N/A";
            // 
            // tb_CodiceArticoloCliente
            // 
            this.tb_CodiceArticoloCliente.Location = new System.Drawing.Point(147, 55);
            this.tb_CodiceArticoloCliente.Name = "tb_CodiceArticoloCliente";
            this.tb_CodiceArticoloCliente.Size = new System.Drawing.Size(200, 20);
            this.tb_CodiceArticoloCliente.TabIndex = 3;
            this.tb_CodiceArticoloCliente.Text = "N/A";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(17, 58);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(113, 13);
            this.label7.TabIndex = 43;
            this.label7.Text = "Codice Articolo Cliente";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(17, 210);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(30, 13);
            this.label8.TabIndex = 42;
            this.label8.Text = "Note";
            // 
            // tb_Articolo
            // 
            this.tb_Articolo.BackColor = System.Drawing.Color.Salmon;
            this.tb_Articolo.Location = new System.Drawing.Point(147, 12);
            this.tb_Articolo.Name = "tb_Articolo";
            this.tb_Articolo.ReadOnly = true;
            this.tb_Articolo.Size = new System.Drawing.Size(552, 20);
            this.tb_Articolo.TabIndex = 1;
            this.tb_Articolo.TextChanged += new System.EventHandler(this.tb_Articolo_TextChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(16, 15);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(42, 13);
            this.label9.TabIndex = 40;
            this.label9.Text = "Articolo";
            // 
            // tabRiparazione
            // 
            this.tabRiparazione.Controls.Add(this.tb_costoAddebitato);
            this.tabRiparazione.Controls.Add(this.tb_costoSostenuto);
            this.tabRiparazione.Controls.Add(this.tb_costoPreventivato);
            this.tabRiparazione.Controls.Add(this.label17);
            this.tabRiparazione.Controls.Add(this.dtp_DataRiparazione);
            this.tabRiparazione.Controls.Add(this.label14);
            this.tabRiparazione.Controls.Add(this.label15);
            this.tabRiparazione.Controls.Add(this.label16);
            this.tabRiparazione.Controls.Add(this.cmb_statoRiparazione);
            this.tabRiparazione.Controls.Add(this.tb_noteRiparazione);
            this.tabRiparazione.Controls.Add(this.label12);
            this.tabRiparazione.Controls.Add(this.label13);
            this.tabRiparazione.Controls.Add(this.cmb_CausaDifetto);
            this.tabRiparazione.Controls.Add(this.cmb_tipoRiparazione);
            this.tabRiparazione.Controls.Add(this.tb_dettaglioRiparazione);
            this.tabRiparazione.Controls.Add(this.label2);
            this.tabRiparazione.Controls.Add(this.label11);
            this.tabRiparazione.Controls.Add(this.label6);
            this.tabRiparazione.Controls.Add(this.tb_difettoRiscontrato);
            this.tabRiparazione.Controls.Add(this.label10);
            this.tabRiparazione.ImageIndex = 2;
            this.tabRiparazione.Location = new System.Drawing.Point(4, 47);
            this.tabRiparazione.Name = "tabRiparazione";
            this.tabRiparazione.Padding = new System.Windows.Forms.Padding(3);
            this.tabRiparazione.Size = new System.Drawing.Size(777, 284);
            this.tabRiparazione.TabIndex = 2;
            this.tabRiparazione.ToolTipText = "Dettagli Riparazione";
            this.tabRiparazione.UseVisualStyleBackColor = true;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(400, 184);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(89, 13);
            this.label17.TabIndex = 80;
            this.label17.Text = "Data Riparazione";
            // 
            // dtp_DataRiparazione
            // 
            this.dtp_DataRiparazione.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtp_DataRiparazione.Location = new System.Drawing.Point(557, 181);
            this.dtp_DataRiparazione.Name = "dtp_DataRiparazione";
            this.dtp_DataRiparazione.Size = new System.Drawing.Size(200, 20);
            this.dtp_DataRiparazione.TabIndex = 9;
            this.dtp_DataRiparazione.Value = new System.DateTime(2019, 1, 20, 11, 14, 2, 445);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(554, 146);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(88, 13);
            this.label14.TabIndex = 75;
            this.label14.Text = "Costo Addebitato";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(16, 146);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(97, 13);
            this.label15.TabIndex = 74;
            this.label15.Text = "Costo Preventivato";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(284, 146);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(85, 13);
            this.label16.TabIndex = 73;
            this.label16.Text = "Costo Sostenuto";
            // 
            // cmb_statoRiparazione
            // 
            this.cmb_statoRiparazione.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cmb_statoRiparazione.FormattingEnabled = true;
            this.cmb_statoRiparazione.Location = new System.Drawing.Point(147, 181);
            this.cmb_statoRiparazione.Name = "cmb_statoRiparazione";
            this.cmb_statoRiparazione.Size = new System.Drawing.Size(200, 21);
            this.cmb_statoRiparazione.Sorted = true;
            this.cmb_statoRiparazione.TabIndex = 8;
            // 
            // tb_noteRiparazione
            // 
            this.tb_noteRiparazione.Location = new System.Drawing.Point(147, 219);
            this.tb_noteRiparazione.Multiline = true;
            this.tb_noteRiparazione.Name = "tb_noteRiparazione";
            this.tb_noteRiparazione.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.tb_noteRiparazione.Size = new System.Drawing.Size(610, 52);
            this.tb_noteRiparazione.TabIndex = 10;
            this.tb_noteRiparazione.Text = "N/A";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(16, 239);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(89, 13);
            this.label12.TabIndex = 66;
            this.label12.Text = "Note Riparazione";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(16, 184);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(91, 13);
            this.label13.TabIndex = 65;
            this.label13.Text = "Stato Riparazione";
            // 
            // cmb_CausaDifetto
            // 
            this.cmb_CausaDifetto.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cmb_CausaDifetto.FormattingEnabled = true;
            this.cmb_CausaDifetto.Location = new System.Drawing.Point(147, 26);
            this.cmb_CausaDifetto.Name = "cmb_CausaDifetto";
            this.cmb_CausaDifetto.Size = new System.Drawing.Size(200, 21);
            this.cmb_CausaDifetto.Sorted = true;
            this.cmb_CausaDifetto.TabIndex = 1;
            // 
            // cmb_tipoRiparazione
            // 
            this.cmb_tipoRiparazione.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cmb_tipoRiparazione.FormattingEnabled = true;
            this.cmb_tipoRiparazione.Location = new System.Drawing.Point(147, 86);
            this.cmb_tipoRiparazione.Name = "cmb_tipoRiparazione";
            this.cmb_tipoRiparazione.Size = new System.Drawing.Size(200, 21);
            this.cmb_tipoRiparazione.Sorted = true;
            this.cmb_tipoRiparazione.TabIndex = 3;
            // 
            // tb_dettaglioRiparazione
            // 
            this.tb_dettaglioRiparazione.Location = new System.Drawing.Point(557, 74);
            this.tb_dettaglioRiparazione.Multiline = true;
            this.tb_dettaglioRiparazione.Name = "tb_dettaglioRiparazione";
            this.tb_dettaglioRiparazione.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.tb_dettaglioRiparazione.Size = new System.Drawing.Size(200, 50);
            this.tb_dettaglioRiparazione.TabIndex = 4;
            this.tb_dettaglioRiparazione.Text = "N/A";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(400, 89);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(108, 13);
            this.label2.TabIndex = 61;
            this.label2.Text = "Dettaglio Riparazione";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(16, 89);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(87, 13);
            this.label11.TabIndex = 60;
            this.label11.Text = "Tipo Riparazione";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(16, 29);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(71, 13);
            this.label6.TabIndex = 57;
            this.label6.Text = "Causa Difetto";
            // 
            // tb_difettoRiscontrato
            // 
            this.tb_difettoRiscontrato.Location = new System.Drawing.Point(557, 10);
            this.tb_difettoRiscontrato.Multiline = true;
            this.tb_difettoRiscontrato.Name = "tb_difettoRiscontrato";
            this.tb_difettoRiscontrato.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.tb_difettoRiscontrato.Size = new System.Drawing.Size(200, 50);
            this.tb_difettoRiscontrato.TabIndex = 2;
            this.tb_difettoRiscontrato.Text = "N/A";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(400, 29);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(95, 13);
            this.label10.TabIndex = 56;
            this.label10.Text = "Difetto Riscontrato";
            // 
            // tb_costoPreventivato
            // 
            this.tb_costoPreventivato.InsertKeyMode = System.Windows.Forms.InsertKeyMode.Insert;
            this.tb_costoPreventivato.Location = new System.Drawing.Point(147, 139);
            this.tb_costoPreventivato.Mask = "99999,99";
            this.tb_costoPreventivato.Name = "tb_costoPreventivato";
            this.tb_costoPreventivato.PromptChar = '0';
            this.tb_costoPreventivato.Size = new System.Drawing.Size(60, 20);
            this.tb_costoPreventivato.TabIndex = 5;
            // 
            // tb_costoSostenuto
            // 
            this.tb_costoSostenuto.InsertKeyMode = System.Windows.Forms.InsertKeyMode.Insert;
            this.tb_costoSostenuto.Location = new System.Drawing.Point(403, 139);
            this.tb_costoSostenuto.Mask = "99999,99";
            this.tb_costoSostenuto.Name = "tb_costoSostenuto";
            this.tb_costoSostenuto.PromptChar = '0';
            this.tb_costoSostenuto.Size = new System.Drawing.Size(60, 20);
            this.tb_costoSostenuto.TabIndex = 6;
            // 
            // tb_costoAddebitato
            // 
            this.tb_costoAddebitato.InsertKeyMode = System.Windows.Forms.InsertKeyMode.Insert;
            this.tb_costoAddebitato.Location = new System.Drawing.Point(697, 139);
            this.tb_costoAddebitato.Mask = "99999,99";
            this.tb_costoAddebitato.Name = "tb_costoAddebitato";
            this.tb_costoAddebitato.PromptChar = '0';
            this.tb_costoAddebitato.Size = new System.Drawing.Size(60, 20);
            this.tb_costoAddebitato.TabIndex = 7;
            // 
            // Form_Riparazione
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(799, 409);
            this.Controls.Add(this.tabControl);
            this.Controls.Add(this.btn_Cancel);
            this.Controls.Add(this.btn_OK);
            this.MaximizeBox = false;
            this.Name = "Form_Riparazione";
            this.Text = "Riparazione";
            this.tabControl.ResumeLayout(false);
            this.tabCliente.ResumeLayout(false);
            this.tabCliente.PerformLayout();
            this.tabArticolo.ResumeLayout(false);
            this.tabArticolo.PerformLayout();
            this.tabRiparazione.ResumeLayout(false);
            this.tabRiparazione.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btn_OK;
        private System.Windows.Forms.ImageList imageList;
        private System.Windows.Forms.TextBox tb_NomeCliente;
        private System.Windows.Forms.Button btn_Cancel;
        private System.Windows.Forms.TabControl tabControl;
        private System.Windows.Forms.TabPage tabCliente;
        private System.Windows.Forms.TabPage tabArticolo;
        private System.Windows.Forms.TabPage tabRiparazione;
        private System.Windows.Forms.Label lbl_dataDocReso;
        private System.Windows.Forms.DateTimePicker dtp_dataDocReso;
        private System.Windows.Forms.DateTimePicker dtp_dataOrdRip;
        private System.Windows.Forms.Label lbl_dataOrdRip;
        private System.Windows.Forms.TextBox tb_numScarCLI;
        private System.Windows.Forms.Label lbl_numSCARcli;
        private System.Windows.Forms.Label lbl_ordRip;
        private System.Windows.Forms.TextBox tb_ordRiparaz;
        private System.Windows.Forms.TextBox tb_difettoSegnalatoDalCliente;
        private System.Windows.Forms.TextBox tb_docReso;
        private System.Windows.Forms.TextBox tb_scarCliente;
        private System.Windows.Forms.Label lbl_docReso;
        private System.Windows.Forms.Label lbl_SCARcli;
        private System.Windows.Forms.Label lbl_difRiscCli;
        private System.Windows.Forms.Button btn_cercaCliente;
        private System.Windows.Forms.ImageList imageList_Tiny;
        private System.Windows.Forms.Button btn_cercaArticolo;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox tb_VersioneArticolo;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox tb_SerialeArticolo;
        private System.Windows.Forms.TextBox tb_NoteArticolo;
        private System.Windows.Forms.TextBox tb_CodiceArticoloCliente;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox tb_Articolo;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox tb_LottoArticolo;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox tb_difettoRiscontrato;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox tb_dettaglioRiparazione;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ComboBox cmb_tipoRiparazione;
        private System.Windows.Forms.ComboBox cmb_CausaDifetto;
        private System.Windows.Forms.ComboBox cmb_statoRiparazione;
        private System.Windows.Forms.TextBox tb_noteRiparazione;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.DateTimePicker dtp_DataRiparazione;
        private System.Windows.Forms.MaskedTextBox tb_costoAddebitato;
        private System.Windows.Forms.MaskedTextBox tb_costoSostenuto;
        private System.Windows.Forms.MaskedTextBox tb_costoPreventivato;
    }
}