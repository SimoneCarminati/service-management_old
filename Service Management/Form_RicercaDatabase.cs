﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Service_Management
{
    public partial class Form_RicercaDatabase : Form
    {
        public int indexRecordToEdit = -1;
        string selectCondition = "";
        string tableName = "";
        public Form_RicercaDatabase(string tableName)
        {
            InitializeComponent();
            dgv_Database.DataSource = Utility.aggiornaDataGridView("SELECT * FROM [" + tableName + "]");
        }

        private void tstb_cercaElemento_Click(object sender, EventArgs e)
        {
            try
            {
                tstb_cercaElemento.Text = "";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void tstb_cercaElemento_Leave(object sender, EventArgs e)
        {
            try
            {
                if (tstb_cercaElemento.Text.Equals(""))
                {
                    tstb_cercaElemento.Text = "Cerca...";
                    dgv_Database.DataSource = Utility.aggiornaDataGridView("SELECT * FROM [" + tableName + "]");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void tstb_cercaElemento_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (!tstb_cercaElemento.Text.Equals("") && !tstb_cercaElemento.Text.Equals("Cerca..."))
                {
                    selectCondition = Utility.getSQLCondition_ForeachColumn(tableName, tstb_cercaElemento.Text);
                    dgv_Database.DataSource = Utility.aggiornaDataGridView("SELECT * FROM [" + tableName + "]" + selectCondition);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dgv_Database_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (dgv_Database.Rows[dgv_Database.CurrentCell.RowIndex].Cells["Id"].Value.ToString() != "" && dgv_Database.SelectedCells.Count.Equals(dgv_Database.Rows[dgv_Database.CurrentCell.RowIndex].Cells.Count))
                {
                    indexRecordToEdit = Convert.ToInt32(dgv_Database.Rows[dgv_Database.CurrentCell.RowIndex].Cells["Id"].Value.ToString());
                    this.DialogResult = DialogResult.OK;
                    this.Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

    }
}
