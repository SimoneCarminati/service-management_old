﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Service_Management
{
    public partial class Form_Articolo : Form
    {
        private int indexRecordToEdit = -1;
        public Form_Articolo()
        {
            try
            {
                InitializeComponent();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// Overload del costruttore con 0 parametri, richiamato tramite :this()
        /// </summary>
        /// <param name="indexRecordToEdit"></param>
        public Form_Articolo(int indexRecordToEdit) : this()
        {
            this.indexRecordToEdit = indexRecordToEdit;
            fillForm();
        }

        private void fillForm()
        {
            DataTable dt = Utility.getDataTableFromDB("SELECT * FROM [Articoli] WHERE [Id] = '" + indexRecordToEdit.ToString() + "'");
            if (!dt.Rows.Count.Equals(0))
            {
                tb_Descrizione.Text = dt.Rows[0].Field<string>("Descrizione");
                tb_Prezzo.Text = dt.Rows[0].Field<decimal>("Prezzo").ToString().PadLeft(8,'0');
            }
        }

        private void btn_OK_Click(object sender, EventArgs e)
        {
            try
            {
                if (tb_Descrizione.Text.Equals(""))
                    MessageBox.Show("Completare i campi obbligatori prima di procedere", "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
                else
                {
                    if (indexRecordToEdit.Equals(-1))
                        searchRecordInDB();
                    else
                        updateRecordInDB();
                    if (this.DialogResult.Equals(DialogResult.OK))
                        this.Dispose();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void searchRecordInDB()
        {
            try
            {
                DataTable dt = Utility.getDataTableFromDB("SELECT * FROM [Articoli] WHERE " +
                       "[Descrizione] = '" + tb_Descrizione.Text + "'");
                if (!dt.Rows.Count.Equals(0))
                {
                    DialogResult result = MessageBox.Show("Il record già presente nel Database verrà aggiornato con i dati immessi nel form.\nSei sicuro di voler procedere?", "Aggiornamento", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (result.Equals(DialogResult.Yes))
                    {
                        indexRecordToEdit = dt.Rows[0].Field<int>(0);
                        updateRecordInDB();
                        this.DialogResult = DialogResult.OK;
                    }
                    else
                        this.DialogResult = DialogResult.Abort;
                }
                else
                    insertRecordInDB();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void insertRecordInDB()
        {
            try
            {
                Utility.executeQuery("INSERT INTO [Articoli] " +
                "([Descrizione],[Prezzo]) " +
                "VALUES ('" + tb_Descrizione.Text + "'," +
                    "'" + tb_Prezzo.Text.Replace(' ', '0') + "')");
                this.DialogResult = DialogResult.OK;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void updateRecordInDB()
        {
            try
            {
                Utility.executeQuery("UPDATE [Articoli] SET " +
                "[Descrizione] = '" + tb_Descrizione.Text + "'," +
                "[Prezzo] = '" + tb_Prezzo.Text.Trim().Replace(' ', '0') + "'" +
                "WHERE [Id] = '" + indexRecordToEdit.ToString() + "'");
                this.DialogResult = DialogResult.OK;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_Cancel_Click(object sender, EventArgs e)
        {
            try
            {
                this.DialogResult = DialogResult.Cancel;
                this.Dispose();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void tb_Descrizione_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (tb_Descrizione.Text.Equals(""))
                    tb_Descrizione.BackColor = Color.Salmon;
                else
                    tb_Descrizione.BackColor = Color.White;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
