﻿namespace Service_Management
{
    partial class Form_Cliente
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form_Cliente));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.tb_CodiceFiscale = new System.Windows.Forms.TextBox();
            this.tb_PIVA = new System.Windows.Forms.TextBox();
            this.tb_Nome = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.tb_Provincia = new System.Windows.Forms.TextBox();
            this.tb_Stato = new System.Windows.Forms.TextBox();
            this.tb_CAP = new System.Windows.Forms.TextBox();
            this.tb_Paese = new System.Windows.Forms.TextBox();
            this.tb_Indirizzo = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.tb_eMail = new System.Windows.Forms.TextBox();
            this.tb_SitoInternet = new System.Windows.Forms.TextBox();
            this.tb_Fax = new System.Windows.Forms.TextBox();
            this.tb_Telefono = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.btn_OK = new System.Windows.Forms.Button();
            this.imageList = new System.Windows.Forms.ImageList(this.components);
            this.btn_Cancel = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.tb_CodiceFiscale);
            this.groupBox1.Controls.Add(this.tb_PIVA);
            this.groupBox1.Controls.Add(this.tb_Nome);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(3, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(667, 101);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Anagrafica";
            // 
            // tb_CodiceFiscale
            // 
            this.tb_CodiceFiscale.Location = new System.Drawing.Point(440, 60);
            this.tb_CodiceFiscale.Name = "tb_CodiceFiscale";
            this.tb_CodiceFiscale.Size = new System.Drawing.Size(211, 20);
            this.tb_CodiceFiscale.TabIndex = 3;
            // 
            // tb_PIVA
            // 
            this.tb_PIVA.Location = new System.Drawing.Point(78, 60);
            this.tb_PIVA.Name = "tb_PIVA";
            this.tb_PIVA.Size = new System.Drawing.Size(270, 20);
            this.tb_PIVA.TabIndex = 2;
            // 
            // tb_Nome
            // 
            this.tb_Nome.BackColor = System.Drawing.Color.Salmon;
            this.tb_Nome.Location = new System.Drawing.Point(78, 28);
            this.tb_Nome.Name = "tb_Nome";
            this.tb_Nome.Size = new System.Drawing.Size(573, 20);
            this.tb_Nome.TabIndex = 1;
            this.tb_Nome.TextChanged += new System.EventHandler(this.tb_Nome_TextChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(358, 63);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(76, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Codice Fiscale";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(9, 63);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(37, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "P. IVA";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 31);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Nome";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.tb_Provincia);
            this.groupBox2.Controls.Add(this.tb_Stato);
            this.groupBox2.Controls.Add(this.tb_CAP);
            this.groupBox2.Controls.Add(this.tb_Paese);
            this.groupBox2.Controls.Add(this.tb_Indirizzo);
            this.groupBox2.Controls.Add(this.label12);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Location = new System.Drawing.Point(3, 110);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(667, 101);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Recapiti";
            // 
            // tb_Provincia
            // 
            this.tb_Provincia.Location = new System.Drawing.Point(520, 28);
            this.tb_Provincia.Name = "tb_Provincia";
            this.tb_Provincia.Size = new System.Drawing.Size(131, 20);
            this.tb_Provincia.TabIndex = 5;
            // 
            // tb_Stato
            // 
            this.tb_Stato.Location = new System.Drawing.Point(501, 60);
            this.tb_Stato.Name = "tb_Stato";
            this.tb_Stato.Size = new System.Drawing.Size(150, 20);
            this.tb_Stato.TabIndex = 8;
            // 
            // tb_CAP
            // 
            this.tb_CAP.Location = new System.Drawing.Point(392, 60);
            this.tb_CAP.Name = "tb_CAP";
            this.tb_CAP.Size = new System.Drawing.Size(65, 20);
            this.tb_CAP.TabIndex = 7;
            // 
            // tb_Paese
            // 
            this.tb_Paese.Location = new System.Drawing.Point(78, 60);
            this.tb_Paese.Name = "tb_Paese";
            this.tb_Paese.Size = new System.Drawing.Size(270, 20);
            this.tb_Paese.TabIndex = 6;
            // 
            // tb_Indirizzo
            // 
            this.tb_Indirizzo.Location = new System.Drawing.Point(78, 28);
            this.tb_Indirizzo.Name = "tb_Indirizzo";
            this.tb_Indirizzo.Size = new System.Drawing.Size(367, 20);
            this.tb_Indirizzo.TabIndex = 4;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(463, 63);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(32, 13);
            this.label12.TabIndex = 4;
            this.label12.Text = "Stato";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(463, 31);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(51, 13);
            this.label10.TabIndex = 3;
            this.label10.Text = "Provincia";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(358, 63);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(28, 13);
            this.label4.TabIndex = 2;
            this.label4.Text = "CAP";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(9, 63);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(37, 13);
            this.label5.TabIndex = 1;
            this.label5.Text = "Paese";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(9, 31);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(45, 13);
            this.label6.TabIndex = 0;
            this.label6.Text = "Indirizzo";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.tb_eMail);
            this.groupBox3.Controls.Add(this.tb_SitoInternet);
            this.groupBox3.Controls.Add(this.tb_Fax);
            this.groupBox3.Controls.Add(this.tb_Telefono);
            this.groupBox3.Controls.Add(this.label11);
            this.groupBox3.Controls.Add(this.label7);
            this.groupBox3.Controls.Add(this.label8);
            this.groupBox3.Controls.Add(this.label9);
            this.groupBox3.Location = new System.Drawing.Point(3, 217);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(667, 101);
            this.groupBox3.TabIndex = 2;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Contatti";
            // 
            // tb_eMail
            // 
            this.tb_eMail.Location = new System.Drawing.Point(301, 60);
            this.tb_eMail.Name = "tb_eMail";
            this.tb_eMail.Size = new System.Drawing.Size(350, 20);
            this.tb_eMail.TabIndex = 12;
            // 
            // tb_SitoInternet
            // 
            this.tb_SitoInternet.Location = new System.Drawing.Point(301, 28);
            this.tb_SitoInternet.Name = "tb_SitoInternet";
            this.tb_SitoInternet.Size = new System.Drawing.Size(350, 20);
            this.tb_SitoInternet.TabIndex = 10;
            // 
            // tb_Fax
            // 
            this.tb_Fax.Location = new System.Drawing.Point(78, 60);
            this.tb_Fax.Name = "tb_Fax";
            this.tb_Fax.Size = new System.Drawing.Size(139, 20);
            this.tb_Fax.TabIndex = 11;
            // 
            // tb_Telefono
            // 
            this.tb_Telefono.Location = new System.Drawing.Point(78, 28);
            this.tb_Telefono.Name = "tb_Telefono";
            this.tb_Telefono.Size = new System.Drawing.Size(139, 20);
            this.tb_Telefono.TabIndex = 9;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(223, 31);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(64, 13);
            this.label11.TabIndex = 3;
            this.label11.Text = "Sito Internet";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(223, 63);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(32, 13);
            this.label7.TabIndex = 2;
            this.label7.Text = "eMail";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(9, 63);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(24, 13);
            this.label8.TabIndex = 1;
            this.label8.Text = "Fax";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(9, 31);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(49, 13);
            this.label9.TabIndex = 0;
            this.label9.Text = "Telefono";
            // 
            // btn_OK
            // 
            this.btn_OK.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btn_OK.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btn_OK.ImageIndex = 0;
            this.btn_OK.ImageList = this.imageList;
            this.btn_OK.Location = new System.Drawing.Point(245, 325);
            this.btn_OK.Name = "btn_OK";
            this.btn_OK.Size = new System.Drawing.Size(47, 47);
            this.btn_OK.TabIndex = 13;
            this.btn_OK.UseVisualStyleBackColor = true;
            this.btn_OK.Click += new System.EventHandler(this.btn_OK_Click);
            // 
            // imageList
            // 
            this.imageList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList.ImageStream")));
            this.imageList.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList.Images.SetKeyName(0, "ok.png");
            this.imageList.Images.SetKeyName(1, "cancel.png");
            // 
            // btn_Cancel
            // 
            this.btn_Cancel.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btn_Cancel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btn_Cancel.ImageIndex = 1;
            this.btn_Cancel.ImageList = this.imageList;
            this.btn_Cancel.Location = new System.Drawing.Point(352, 325);
            this.btn_Cancel.Name = "btn_Cancel";
            this.btn_Cancel.Size = new System.Drawing.Size(47, 47);
            this.btn_Cancel.TabIndex = 14;
            this.btn_Cancel.UseVisualStyleBackColor = true;
            this.btn_Cancel.Click += new System.EventHandler(this.btn_Cancel_Click);
            // 
            // Form_Cliente
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(676, 378);
            this.Controls.Add(this.btn_Cancel);
            this.Controls.Add(this.btn_OK);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.MaximizeBox = false;
            this.Name = "Form_Cliente";
            this.Text = "Cliente";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Button btn_OK;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.ImageList imageList;
        private System.Windows.Forms.TextBox tb_CodiceFiscale;
        private System.Windows.Forms.TextBox tb_PIVA;
        private System.Windows.Forms.TextBox tb_Nome;
        private System.Windows.Forms.TextBox tb_Provincia;
        private System.Windows.Forms.TextBox tb_Stato;
        private System.Windows.Forms.TextBox tb_CAP;
        private System.Windows.Forms.TextBox tb_Paese;
        private System.Windows.Forms.TextBox tb_Indirizzo;
        private System.Windows.Forms.TextBox tb_Fax;
        private System.Windows.Forms.TextBox tb_Telefono;
        private System.Windows.Forms.TextBox tb_eMail;
        private System.Windows.Forms.TextBox tb_SitoInternet;
        private System.Windows.Forms.Button btn_Cancel;
    }
}