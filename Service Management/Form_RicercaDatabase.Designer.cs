﻿namespace Service_Management
{
    partial class Form_RicercaDatabase
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.toolStrip_Riparazioni = new System.Windows.Forms.ToolStrip();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.tstb_cercaElemento = new System.Windows.Forms.ToolStripTextBox();
            this.toolStripSeparator9 = new System.Windows.Forms.ToolStripSeparator();
            this.dgv_Database = new System.Windows.Forms.DataGridView();
            this.toolStrip_Riparazioni.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_Database)).BeginInit();
            this.SuspendLayout();
            // 
            // toolStrip_Riparazioni
            // 
            this.toolStrip_Riparazioni.AutoSize = false;
            this.toolStrip_Riparazioni.BackColor = System.Drawing.SystemColors.Control;
            this.toolStrip_Riparazioni.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip_Riparazioni.ImageScalingSize = new System.Drawing.Size(47, 47);
            this.toolStrip_Riparazioni.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripSeparator1,
            this.tstb_cercaElemento,
            this.toolStripSeparator9});
            this.toolStrip_Riparazioni.Location = new System.Drawing.Point(0, 0);
            this.toolStrip_Riparazioni.Name = "toolStrip_Riparazioni";
            this.toolStrip_Riparazioni.Size = new System.Drawing.Size(800, 50);
            this.toolStrip_Riparazioni.TabIndex = 1;
            this.toolStrip_Riparazioni.Text = "toolStrip1";
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 50);
            // 
            // tstb_cercaElemento
            // 
            this.tstb_cercaElemento.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.tstb_cercaElemento.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.tstb_cercaElemento.Name = "tstb_cercaElemento";
            this.tstb_cercaElemento.Size = new System.Drawing.Size(100, 50);
            this.tstb_cercaElemento.Text = "Cerca...";
            this.tstb_cercaElemento.ToolTipText = "Cerca Riparazione";
            this.tstb_cercaElemento.Leave += new System.EventHandler(this.tstb_cercaElemento_Leave);
            this.tstb_cercaElemento.Click += new System.EventHandler(this.tstb_cercaElemento_Click);
            this.tstb_cercaElemento.TextChanged += new System.EventHandler(this.tstb_cercaElemento_TextChanged);
            // 
            // toolStripSeparator9
            // 
            this.toolStripSeparator9.Name = "toolStripSeparator9";
            this.toolStripSeparator9.Size = new System.Drawing.Size(6, 50);
            // 
            // dgv_Database
            // 
            this.dgv_Database.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_Database.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgv_Database.Location = new System.Drawing.Point(0, 50);
            this.dgv_Database.Name = "dgv_Database";
            this.dgv_Database.ReadOnly = true;
            this.dgv_Database.Size = new System.Drawing.Size(800, 400);
            this.dgv_Database.TabIndex = 7;
            this.dgv_Database.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_Database_CellClick);
            // 
            // Form_RicercaDatabase
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.dgv_Database);
            this.Controls.Add(this.toolStrip_Riparazioni);
            this.Name = "Form_RicercaDatabase";
            this.Text = "Ricerca Nel Database";
            this.toolStrip_Riparazioni.ResumeLayout(false);
            this.toolStrip_Riparazioni.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_Database)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ToolStrip toolStrip_Riparazioni;
        private System.Windows.Forms.ToolStripTextBox tstb_cercaElemento;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator9;
        private System.Windows.Forms.DataGridView dgv_Database;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
    }
}