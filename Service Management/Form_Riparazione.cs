﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Service_Management
{
    public partial class Form_Riparazione : Form
    {
        private int indexRecordToEdit = -1;
        private Dictionary<String, int> extKeys = new Dictionary<String, int> { { "Id_Cliente", -1 },{ "Id_Articolo", -1 } };
        private DataTable dt_Rip;
        private DataTable dt_Cli;
        private DataTable dt_Art;
        public Form_Riparazione()
        {
            try
            {
                InitializeComponent();
                setDateTimePicker();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// Overload del costruttore con 0 parametri, richiamato tramite :this()
        /// </summary>
        /// <param name="indexRecordToEdit"></param>
        public Form_Riparazione(int indexRecordToEdit) : this()
        {
            this.indexRecordToEdit = indexRecordToEdit;
            fillForm();
        }

        private void setDateTimePicker()
        {
            dtp_dataOrdRip.Value = DateTime.Now;
            dtp_dataDocReso.Value = DateTime.Now;
            dtp_DataRiparazione.Value = DateTime.Now;
        }

        private void fillForm()
        {
            try
            {
                dt_Rip = Utility.getDataTableFromDB("SELECT * FROM [Riparazioni] WHERE [Id] = '" + indexRecordToEdit.ToString() + "'");
                if (dt_Rip.Rows.Count.Equals(1))
                {
                    dt_Cli = Utility.getDataTableFromDB("SELECT * FROM [Clienti] WHERE [Id] = '" + dt_Rip.Rows[0].Field<int>("Id_Cliente").ToString() + "'");
                    dt_Art = Utility.getDataTableFromDB("SELECT * FROM [Articoli] WHERE [Id] = '" + dt_Rip.Rows[0].Field<int>("Id_Articolo").ToString() + "'");
                    tb_NomeCliente.Text = dt_Cli.Rows[0].Field<string>("Nome");
                    tb_scarCliente.Text = dt_Rip.Rows[0].Field<string>("SCAR_Cliente");
                    tb_numScarCLI.Text = dt_Rip.Rows[0].Field<string>("Num_SCAR_Cliente");
                    tb_ordRiparaz.Text = dt_Rip.Rows[0].Field<string>("Ordine_Riparazione");
                    dtp_dataOrdRip.Value = dt_Rip.Rows[0].Field<DateTime>("Data_Ordine_Riparazione");
                    tb_docReso.Text = dt_Rip.Rows[0].Field<string>("Documento_di_Reso");
                    dtp_dataDocReso.Value = dt_Rip.Rows[0].Field<DateTime>("Data_Documento_di_Reso");
                    tb_difettoSegnalatoDalCliente.Text = dt_Rip.Rows[0].Field<string>("Difetto_Segnalato_Dal_Cliente");
                    tb_Articolo.Text = dt_Art.Rows[0].Field<string>("Descrizione");
                    tb_CodiceArticoloCliente.Text = dt_Rip.Rows[0].Field<string>("Codice_Articolo_Cliente");
                    tb_VersioneArticolo.Text = dt_Rip.Rows[0].Field<string>("Versione_Articolo");
                    tb_SerialeArticolo.Text = dt_Rip.Rows[0].Field<string>("Seriale_Articolo");
                    tb_LottoArticolo.Text = dt_Rip.Rows[0].Field<string>("Lotto_Articolo");
                    tb_NoteArticolo.Text = dt_Rip.Rows[0].Field<string>("Note_Articolo");
                    cmb_CausaDifetto.SelectedItem = dt_Rip.Rows[0].Field<string>("Causa_Difetto");
                    tb_difettoRiscontrato.Text = dt_Rip.Rows[0].Field<string>("Difetto_Riscontrato");
                    cmb_tipoRiparazione.SelectedItem = dt_Rip.Rows[0].Field<string>("Tipo_Riparazione");
                    tb_dettaglioRiparazione.Text = dt_Rip.Rows[0].Field<string>("Dettaglio_Riparazione");
                    tb_costoPreventivato.Text = dt_Rip.Rows[0].Field<decimal>("Costo_Preventivato").ToString().PadLeft(8, '0');
                    tb_costoSostenuto.Text = dt_Rip.Rows[0].Field<decimal>("Costo_Sostenuto").ToString().PadLeft(8, '0');
                    tb_costoAddebitato.Text = dt_Rip.Rows[0].Field<decimal>("Costo_Addebitato").ToString().PadLeft(8, '0');
                    cmb_statoRiparazione.SelectedItem = dt_Rip.Rows[0].Field<string>("Stato_Riparazione");
                    dtp_DataRiparazione.Value = dt_Rip.Rows[0].Field<DateTime>("Data_Riparazione");
                    tb_noteRiparazione.Text = dt_Rip.Rows[0].Field<string>("Note_Riparazione");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        #region Cliente
        private void tb_Nome_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (tb_NomeCliente.Text.Equals(""))
                    tb_NomeCliente.BackColor = Color.Salmon;
                else
                    tb_NomeCliente.BackColor = Color.White;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_cercaCliente_Click(object sender, EventArgs e)
        {
            try
            {
                DataTable dt = Utility.getRecordFromDBTable("Clienti");
                if (!dt.Rows.Count.Equals(0))
                {
                    extKeys["Id_Cliente"] = dt.Rows[0].Field<int>("Id");
                    tb_NomeCliente.Text = dt.Rows[0].Field<String>("Nome");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        #endregion

        #region Articolo
        private void tb_Articolo_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (tb_Articolo.Text.Equals(""))
                    tb_Articolo.BackColor = Color.Salmon;
                else
                    tb_Articolo.BackColor = Color.White;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_cercaArticolo_Click(object sender, EventArgs e)
        {
            try
            {
                DataTable dt = Utility.getRecordFromDBTable("Articoli");
                if (!dt.Rows.Count.Equals(0))
                {
                    extKeys["Id_Articolo"] = dt.Rows[0].Field<int>("Id");
                    tb_Articolo.Text = dt.Rows[0].Field<String>("Descrizione");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        #endregion

        private void btn_OK_Click(object sender, EventArgs e)
        {
            try
            {
                if (tb_NomeCliente.Text.Equals("") || tb_Articolo.Text.Equals(""))
                    MessageBox.Show("Completare i campi obbligatori prima di procedere", "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
                else
                {
                    if (indexRecordToEdit.Equals(-1))
                        searchRecordInDB();
                    else
                        updateRecordInDB();
                    if (this.DialogResult.Equals(DialogResult.OK))
                        this.Dispose();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void searchRecordInDB()
        {
            try
            {
                dt_Cli = Utility.getDataTableFromDB("SELECT * FROM [Clienti] WHERE [Nome] = '" + tb_NomeCliente.Text + "'");
                dt_Art = Utility.getDataTableFromDB("SELECT * FROM [Articoli] WHERE [Descrizione] = '" + tb_Articolo.Text + "'");
                DataTable dt = Utility.getDataTableFromDB("SELECT * FROM [Riparazioni] WHERE " +
                       //"[Data_Riparazione] = '" + dtp_DataRiparazione.Value.ToString("yyyy-MM-dd HH:mm:ss.fffffff") +"'");
                       "[Id_Cliente] = '" + dt_Cli.Rows[0].Field<int>("Id") + "' AND " +
                       "[SCAR_Cliente] = '" + tb_scarCliente.Text + "' AND " +
                       "[Num_SCAR_Cliente] = '" + tb_numScarCLI.Text + "' AND " +
                       "[Ordine_Riparazione] = '" + tb_ordRiparaz.Text + "' AND " +
                       "[Data_Ordine_Riparazione] = '" + dtp_dataOrdRip.Value.ToString("yyyy-MM-dd HH:mm:ss.fffffff") + "' AND " +
                       "[Documento_di_Reso] = '" + tb_docReso.Text + "' AND " +
                       "[Data_Documento_di_Reso] = '" + dtp_dataDocReso.Value.ToString("yyyy-MM-dd HH:mm:ss.fffffff") + "' AND " +
                       "[Difetto_Segnalato_Dal_Cliente] = '" + tb_difettoSegnalatoDalCliente.Text + "' AND " +
                       "[Id_Articolo] = '" + dt_Art.Rows[0].Field<int>("Id") + "' AND " +
                       "[Codice_Articolo_Cliente] = '" + tb_CodiceArticoloCliente.Text + "' AND " +
                       "[Versione_Articolo] = '" + tb_VersioneArticolo.Text + "' AND " +
                       "[Seriale_Articolo] = '" + tb_SerialeArticolo.Text + "' AND " +
                       "[Lotto_Articolo] = '" + tb_LottoArticolo.Text + "' AND " +
                       "[Note_Articolo] = '" + tb_NoteArticolo.Text + "' AND " +
                       "[Causa_Difetto] = '" + (cmb_CausaDifetto.SelectedItem != null ? cmb_CausaDifetto.SelectedItem.ToString() : "") + "' AND " +
                       "[Difetto_Riscontrato] = '" + tb_difettoRiscontrato.Text + "' AND " +
                       "[Tipo_Riparazione] = '" + (cmb_tipoRiparazione.SelectedItem != null ? cmb_tipoRiparazione.SelectedItem.ToString() : "") + "' AND " +
                       "[Dettaglio_Riparazione] = '" + tb_dettaglioRiparazione.Text + "' AND " +
                       "[Costo_Preventivato] = '" + tb_costoPreventivato.Text.Replace(' ', '0') + "' AND " +
                       "[Costo_Sostenuto] = '" + tb_costoSostenuto.Text.Replace(' ', '0') + "' AND " +
                       "[Costo_Addebitato] = '" + tb_costoAddebitato.Text.Replace(' ', '0') + "' AND " +
                       "[Stato_Riparazione] = '" + (cmb_tipoRiparazione.SelectedItem != null ? cmb_tipoRiparazione.SelectedItem.ToString() : "") + "' AND " +
                       "[Data_Riparazione] = '" + dtp_DataRiparazione.Value.ToString("yyyy-MM-dd HH:mm:ss.fffffff") + "' AND " +
                       "[Note_Riparazione] = '" + tb_noteRiparazione.Text + "'");
                if (!dt.Rows.Count.Equals(0))
                {
                    DialogResult result = MessageBox.Show("Il record già presente nel Database verrà aggiornato con i dati immessi nel form.\nSei sicuro di voler procedere?", "Aggiornamento", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (result.Equals(DialogResult.Yes))
                    {
                        indexRecordToEdit = dt.Rows[0].Field<int>(0);
                        updateRecordInDB();
                        this.DialogResult = DialogResult.OK;
                    }
                    else
                        this.DialogResult = DialogResult.Abort;
                }
                else
                    insertRecordInDB();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void insertRecordInDB()
        {
            try
            {
                Utility.executeQuery("INSERT INTO [Riparazioni] " +
                "([Id_Cliente],[SCAR_Cliente],[Num_SCAR_Cliente],[Ordine_Riparazione],[Data_Ordine_Riparazione],[Documento_di_Reso],[Data_Documento_di_Reso],[Difetto_Segnalato_Dal_Cliente],[Id_Articolo],[Codice_Articolo_Cliente],[Versione_Articolo],[Seriale_Articolo],[Lotto_Articolo],[Note_Articolo],[Causa_Difetto],[Difetto_Riscontrato],[Tipo_Riparazione],[Dettaglio_Riparazione],[Costo_Preventivato],[Costo_Sostenuto],[Costo_Addebitato],[Stato_Riparazione],[Data_Riparazione],[Note_Riparazione]) " +
                "VALUES ('" + dt_Cli.Rows[0].Field<int>("Id") + "'," +
                    "'" + tb_scarCliente.Text + "'," +
                    "'" + tb_numScarCLI.Text + "'," +
                    "'" + tb_ordRiparaz.Text + "'," +
                    "'" + dtp_dataOrdRip.Value.ToString("yyyy-MM-dd HH:mm:ss.fffffff") + "'," +
                    "'" + tb_docReso.Text + "'," +
                    "'" + dtp_dataDocReso.Value.ToString("yyyy-MM-dd HH:mm:ss.fffffff") + "'," +
                    "'" + tb_difettoSegnalatoDalCliente.Text + "'," +
                    "'" + dt_Art.Rows[0].Field<int>("Id") + "'," +
                    "'" + tb_CodiceArticoloCliente.Text + "'," +
                    "'" + tb_VersioneArticolo.Text + "'," +
                    "'" + tb_SerialeArticolo.Text + "'," +
                    "'" + tb_LottoArticolo.Text + "'," +
                    "'" + tb_NoteArticolo.Text + "'," +
                    "'" + (cmb_CausaDifetto.SelectedItem != null ? cmb_CausaDifetto.SelectedItem.ToString() : "") + "'," +
                    "'" + tb_difettoRiscontrato.Text + "'," +
                    "'" + (cmb_tipoRiparazione.SelectedItem != null ? cmb_tipoRiparazione.SelectedItem.ToString() : "") + "'," +
                    "'" + tb_dettaglioRiparazione.Text + "'," +
                    "'" + tb_costoPreventivato.Text.Replace(' ', '0') + "'," +
                    "'" + tb_costoSostenuto.Text.Replace(' ', '0') + "'," +
                    "'" + tb_costoAddebitato.Text.Replace(' ', '0') + "'," +
                    "'" + (cmb_tipoRiparazione.SelectedItem != null ? cmb_tipoRiparazione.SelectedItem.ToString() : "") + "'," +
                    "'" + dtp_DataRiparazione.Value.ToString("yyyy-MM-dd HH:mm:ss.fffffff") + "'," +
                    "'" + tb_noteRiparazione.Text + "')");
                this.DialogResult = DialogResult.OK;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void updateRecordInDB()
        {
            try
            {
                Utility.executeQuery("UPDATE [Riparazioni] SET " +
                "[Id_Cliente] = '" + dt_Cli.Rows[0].Field<int>("Id") + "'," +
                "[SCAR_Cliente] = '" + tb_scarCliente.Text + "'," +
                "[Num_SCAR_Cliente] = '" + tb_numScarCLI.Text + "'," +
                "[Ordine_Riparazione] = '" + tb_ordRiparaz.Text + "'," +
                "[Data_Ordine_Riparazione] = '" + dtp_dataOrdRip.Value.ToString("yyyy-MM-dd HH:mm:ss.fffffff") + "'," +
                "[Documento_di_Reso] = '" + tb_docReso.Text + "'," +
                "[Data_Documento_di_Reso] = '" + dtp_dataDocReso.Value.ToString("yyyy-MM-dd HH:mm:ss.fffffff") + "'," +
                "[Difetto_Segnalato_Dal_Cliente] = '" + tb_difettoSegnalatoDalCliente.Text + "'," +
                "[Id_Articolo] = '" + dt_Art.Rows[0].Field<int>("Id") + "'," +
                "[Codice_Articolo_Cliente] = '" + tb_CodiceArticoloCliente.Text + "'," +
                "[Versione_Articolo] = '" + tb_VersioneArticolo.Text + "'," +
                "[Seriale_Articolo] = '" + tb_SerialeArticolo.Text + "'," +
                "[Lotto_Articolo] = '" + tb_LottoArticolo.Text + "'," +
                "[Note_Articolo] = '" + tb_NoteArticolo.Text + "'," +
                "[Causa_Difetto] = '" + (cmb_CausaDifetto.SelectedItem != null ? cmb_CausaDifetto.SelectedItem.ToString() : "") + "'," +
                "[Difetto_Riscontrato] = '" + tb_difettoRiscontrato.Text + "'," +
                "[Tipo_Riparazione] = '" + (cmb_tipoRiparazione.SelectedItem != null ? cmb_tipoRiparazione.SelectedItem.ToString() : "") + "'," +
                "[Dettaglio_Riparazione] = '" + tb_dettaglioRiparazione.Text + "'," +
                "[Costo_Preventivato] = '" + tb_costoPreventivato.Text.Replace(' ', '0') + "'," +
                "[Costo_Sostenuto] = '" + tb_costoSostenuto.Text.Replace(' ', '0') + "'," +
                "[Costo_Addebitato] = '" + tb_costoAddebitato.Text.Replace(' ', '0') + "'," +
                "[Stato_Riparazione] = '" + (cmb_tipoRiparazione.SelectedItem != null ? cmb_tipoRiparazione.SelectedItem.ToString() : "") + "'," +
                "[Data_Riparazione] = '" + dtp_DataRiparazione.Value.ToString("yyyy-MM-dd HH:mm:ss.fffffff") + "'," +
                "[Note_Riparazione] = '" + tb_noteRiparazione.Text + "'," +
                "WHERE [Id] = '" + indexRecordToEdit.ToString() + "'");
                this.DialogResult = DialogResult.OK;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_Cancel_Click(object sender, EventArgs e)
        {
            try
            {
                this.DialogResult = DialogResult.Cancel;
                this.Dispose();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

    }
}
