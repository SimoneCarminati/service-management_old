﻿namespace Service_Management
{
    partial class Form_Main
    {
        /// <summary>
        /// Variabile di progettazione necessaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Pulire le risorse in uso.
        /// </summary>
        /// <param name="disposing">ha valore true se le risorse gestite devono essere eliminate, false in caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Codice generato da Progettazione Windows Form

        /// <summary>
        /// Metodo necessario per il supporto della finestra di progettazione. Non modificare
        /// il contenuto del metodo con l'editor di codice.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form_Main));
            this.imageList = new System.Windows.Forms.ImageList(this.components);
            this.tabControl = new System.Windows.Forms.TabControl();
            this.tabRiparazioni = new System.Windows.Forms.TabPage();
            this.dgv_Riparazioni = new System.Windows.Forms.DataGridView();
            this.toolStrip_Riparazioni = new System.Windows.Forms.ToolStrip();
            this.tsbtn_AddRiparazione = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator8 = new System.Windows.Forms.ToolStripSeparator();
            this.tsbtn_RemoveRiparazione = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.tstb_cercaRiparazioni = new System.Windows.Forms.ToolStripTextBox();
            this.toolStripSeparator9 = new System.Windows.Forms.ToolStripSeparator();
            this.tsbtn_ExportRiparazioni = new System.Windows.Forms.ToolStripButton();
            this.tsbtn_PrintRiparazione = new System.Windows.Forms.ToolStripButton();
            this.tabArticoli = new System.Windows.Forms.TabPage();
            this.dgv_Articoli = new System.Windows.Forms.DataGridView();
            this.toolStrip_Prodotti = new System.Windows.Forms.ToolStrip();
            this.tsbtn_AddArticolo = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.tsbtn_RemoveArticolo = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.tstb_cercaArticoli = new System.Windows.Forms.ToolStripTextBox();
            this.toolStripSeparator7 = new System.Windows.Forms.ToolStripSeparator();
            this.tsbtn_ExportArticoli = new System.Windows.Forms.ToolStripButton();
            this.tabClienti = new System.Windows.Forms.TabPage();
            this.dgv_Clienti = new System.Windows.Forms.DataGridView();
            this.toolStrip_Clienti = new System.Windows.Forms.ToolStrip();
            this.tsbtn_AddCliente = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.tsbtn_RemoveCliente = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.tstb_cercaClienti = new System.Windows.Forms.ToolStripTextBox();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.tsbtn_ExportClienti = new System.Windows.Forms.ToolStripButton();
            this.tabControl.SuspendLayout();
            this.tabRiparazioni.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_Riparazioni)).BeginInit();
            this.toolStrip_Riparazioni.SuspendLayout();
            this.tabArticoli.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_Articoli)).BeginInit();
            this.toolStrip_Prodotti.SuspendLayout();
            this.tabClienti.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_Clienti)).BeginInit();
            this.toolStrip_Clienti.SuspendLayout();
            this.SuspendLayout();
            // 
            // imageList
            // 
            this.imageList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList.ImageStream")));
            this.imageList.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList.Images.SetKeyName(0, "boards.png");
            this.imageList.Images.SetKeyName(1, "clients.png");
            this.imageList.Images.SetKeyName(2, "repairs.png");
            // 
            // tabControl
            // 
            this.tabControl.Appearance = System.Windows.Forms.TabAppearance.Buttons;
            this.tabControl.Controls.Add(this.tabRiparazioni);
            this.tabControl.Controls.Add(this.tabArticoli);
            this.tabControl.Controls.Add(this.tabClienti);
            this.tabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl.ImageList = this.imageList;
            this.tabControl.Location = new System.Drawing.Point(0, 0);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.ShowToolTips = true;
            this.tabControl.Size = new System.Drawing.Size(1242, 512);
            this.tabControl.TabIndex = 0;
            // 
            // tabRiparazioni
            // 
            this.tabRiparazioni.Controls.Add(this.dgv_Riparazioni);
            this.tabRiparazioni.Controls.Add(this.toolStrip_Riparazioni);
            this.tabRiparazioni.ImageIndex = 2;
            this.tabRiparazioni.Location = new System.Drawing.Point(4, 60);
            this.tabRiparazioni.Name = "tabRiparazioni";
            this.tabRiparazioni.Padding = new System.Windows.Forms.Padding(3);
            this.tabRiparazioni.Size = new System.Drawing.Size(1234, 448);
            this.tabRiparazioni.TabIndex = 0;
            this.tabRiparazioni.ToolTipText = "Riparazioni";
            this.tabRiparazioni.UseVisualStyleBackColor = true;
            // 
            // dgv_Riparazioni
            // 
            this.dgv_Riparazioni.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_Riparazioni.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgv_Riparazioni.Location = new System.Drawing.Point(3, 53);
            this.dgv_Riparazioni.Name = "dgv_Riparazioni";
            this.dgv_Riparazioni.ReadOnly = true;
            this.dgv_Riparazioni.Size = new System.Drawing.Size(1228, 392);
            this.dgv_Riparazioni.TabIndex = 6;
            // 
            // toolStrip_Riparazioni
            // 
            this.toolStrip_Riparazioni.AutoSize = false;
            this.toolStrip_Riparazioni.BackColor = System.Drawing.SystemColors.Control;
            this.toolStrip_Riparazioni.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip_Riparazioni.ImageScalingSize = new System.Drawing.Size(47, 47);
            this.toolStrip_Riparazioni.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsbtn_AddRiparazione,
            this.toolStripSeparator8,
            this.tsbtn_RemoveRiparazione,
            this.toolStripSeparator1,
            this.tstb_cercaRiparazioni,
            this.toolStripSeparator9,
            this.tsbtn_ExportRiparazioni,
            this.tsbtn_PrintRiparazione});
            this.toolStrip_Riparazioni.Location = new System.Drawing.Point(3, 3);
            this.toolStrip_Riparazioni.Name = "toolStrip_Riparazioni";
            this.toolStrip_Riparazioni.Size = new System.Drawing.Size(1228, 50);
            this.toolStrip_Riparazioni.TabIndex = 0;
            this.toolStrip_Riparazioni.Text = "toolStrip1";
            // 
            // tsbtn_AddRiparazione
            // 
            this.tsbtn_AddRiparazione.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.tsbtn_AddRiparazione.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbtn_AddRiparazione.Image = ((System.Drawing.Image)(resources.GetObject("tsbtn_AddRiparazione.Image")));
            this.tsbtn_AddRiparazione.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbtn_AddRiparazione.Name = "tsbtn_AddRiparazione";
            this.tsbtn_AddRiparazione.Size = new System.Drawing.Size(51, 47);
            this.tsbtn_AddRiparazione.ToolTipText = "Aggiungi Riparazione";
            this.tsbtn_AddRiparazione.Click += new System.EventHandler(this.tsbtn_AddRiparazione_Click);
            // 
            // toolStripSeparator8
            // 
            this.toolStripSeparator8.Name = "toolStripSeparator8";
            this.toolStripSeparator8.Size = new System.Drawing.Size(6, 50);
            // 
            // tsbtn_RemoveRiparazione
            // 
            this.tsbtn_RemoveRiparazione.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbtn_RemoveRiparazione.Image = ((System.Drawing.Image)(resources.GetObject("tsbtn_RemoveRiparazione.Image")));
            this.tsbtn_RemoveRiparazione.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbtn_RemoveRiparazione.Name = "tsbtn_RemoveRiparazione";
            this.tsbtn_RemoveRiparazione.Size = new System.Drawing.Size(51, 47);
            this.tsbtn_RemoveRiparazione.Text = "toolStripButton2";
            this.tsbtn_RemoveRiparazione.ToolTipText = "Rimuovi Riparazione";
            this.tsbtn_RemoveRiparazione.Click += new System.EventHandler(this.tsbtn_RemoveRiparazione_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 50);
            // 
            // tstb_cercaRiparazioni
            // 
            this.tstb_cercaRiparazioni.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.tstb_cercaRiparazioni.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.tstb_cercaRiparazioni.Name = "tstb_cercaRiparazioni";
            this.tstb_cercaRiparazioni.Size = new System.Drawing.Size(100, 50);
            this.tstb_cercaRiparazioni.Text = "Cerca...";
            this.tstb_cercaRiparazioni.ToolTipText = "Cerca Riparazione";
            this.tstb_cercaRiparazioni.Leave += new System.EventHandler(this.tstb_cercaRiparazioni_Leave);
            this.tstb_cercaRiparazioni.Click += new System.EventHandler(this.tstb_cercaRiparazioni_Click);
            this.tstb_cercaRiparazioni.TextChanged += new System.EventHandler(this.tstb_cercaRiparazioni_TextChanged);
            // 
            // toolStripSeparator9
            // 
            this.toolStripSeparator9.Name = "toolStripSeparator9";
            this.toolStripSeparator9.Size = new System.Drawing.Size(6, 50);
            // 
            // tsbtn_ExportRiparazioni
            // 
            this.tsbtn_ExportRiparazioni.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbtn_ExportRiparazioni.Image = ((System.Drawing.Image)(resources.GetObject("tsbtn_ExportRiparazioni.Image")));
            this.tsbtn_ExportRiparazioni.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbtn_ExportRiparazioni.Name = "tsbtn_ExportRiparazioni";
            this.tsbtn_ExportRiparazioni.Size = new System.Drawing.Size(51, 47);
            this.tsbtn_ExportRiparazioni.Text = "toolStripButton4";
            this.tsbtn_ExportRiparazioni.ToolTipText = "Esporta Riparazioni";
            this.tsbtn_ExportRiparazioni.Click += new System.EventHandler(this.tsbtn_ExportRiparazioni_Click);
            // 
            // tsbtn_PrintRiparazione
            // 
            this.tsbtn_PrintRiparazione.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbtn_PrintRiparazione.Image = ((System.Drawing.Image)(resources.GetObject("tsbtn_PrintRiparazione.Image")));
            this.tsbtn_PrintRiparazione.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbtn_PrintRiparazione.Name = "tsbtn_PrintRiparazione";
            this.tsbtn_PrintRiparazione.Size = new System.Drawing.Size(51, 47);
            this.tsbtn_PrintRiparazione.ToolTipText = "Stampa Riparazione";
            this.tsbtn_PrintRiparazione.Click += new System.EventHandler(this.tsbtn_PrintRiparazione_Click);
            // 
            // tabArticoli
            // 
            this.tabArticoli.Controls.Add(this.dgv_Articoli);
            this.tabArticoli.Controls.Add(this.toolStrip_Prodotti);
            this.tabArticoli.ImageIndex = 0;
            this.tabArticoli.Location = new System.Drawing.Point(4, 60);
            this.tabArticoli.Name = "tabArticoli";
            this.tabArticoli.Padding = new System.Windows.Forms.Padding(3);
            this.tabArticoli.Size = new System.Drawing.Size(1234, 448);
            this.tabArticoli.TabIndex = 1;
            this.tabArticoli.ToolTipText = "Articoli";
            this.tabArticoli.UseVisualStyleBackColor = true;
            // 
            // dgv_Articoli
            // 
            this.dgv_Articoli.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_Articoli.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgv_Articoli.Location = new System.Drawing.Point(3, 53);
            this.dgv_Articoli.Name = "dgv_Articoli";
            this.dgv_Articoli.ReadOnly = true;
            this.dgv_Articoli.Size = new System.Drawing.Size(1228, 392);
            this.dgv_Articoli.TabIndex = 6;
            this.dgv_Articoli.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_Articoli_CellClick);
            // 
            // toolStrip_Prodotti
            // 
            this.toolStrip_Prodotti.AutoSize = false;
            this.toolStrip_Prodotti.BackColor = System.Drawing.SystemColors.Control;
            this.toolStrip_Prodotti.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip_Prodotti.ImageScalingSize = new System.Drawing.Size(47, 47);
            this.toolStrip_Prodotti.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsbtn_AddArticolo,
            this.toolStripSeparator4,
            this.tsbtn_RemoveArticolo,
            this.toolStripSeparator3,
            this.tstb_cercaArticoli,
            this.toolStripSeparator7,
            this.tsbtn_ExportArticoli});
            this.toolStrip_Prodotti.Location = new System.Drawing.Point(3, 3);
            this.toolStrip_Prodotti.Name = "toolStrip_Prodotti";
            this.toolStrip_Prodotti.Size = new System.Drawing.Size(1228, 50);
            this.toolStrip_Prodotti.TabIndex = 3;
            this.toolStrip_Prodotti.Text = "toolStrip3";
            // 
            // tsbtn_AddArticolo
            // 
            this.tsbtn_AddArticolo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.tsbtn_AddArticolo.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbtn_AddArticolo.Image = ((System.Drawing.Image)(resources.GetObject("tsbtn_AddArticolo.Image")));
            this.tsbtn_AddArticolo.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbtn_AddArticolo.Name = "tsbtn_AddArticolo";
            this.tsbtn_AddArticolo.Size = new System.Drawing.Size(51, 47);
            this.tsbtn_AddArticolo.ToolTipText = "Aggiungi Articolo";
            this.tsbtn_AddArticolo.Click += new System.EventHandler(this.tsbtn_AddArticolo_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(6, 50);
            // 
            // tsbtn_RemoveArticolo
            // 
            this.tsbtn_RemoveArticolo.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbtn_RemoveArticolo.Image = ((System.Drawing.Image)(resources.GetObject("tsbtn_RemoveArticolo.Image")));
            this.tsbtn_RemoveArticolo.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbtn_RemoveArticolo.Name = "tsbtn_RemoveArticolo";
            this.tsbtn_RemoveArticolo.Size = new System.Drawing.Size(51, 47);
            this.tsbtn_RemoveArticolo.Text = "toolStripButton2";
            this.tsbtn_RemoveArticolo.ToolTipText = "Rimuovi Articolo";
            this.tsbtn_RemoveArticolo.Click += new System.EventHandler(this.tsbtn_RemoveArticolo_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 50);
            // 
            // tstb_cercaArticoli
            // 
            this.tstb_cercaArticoli.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.tstb_cercaArticoli.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.tstb_cercaArticoli.Name = "tstb_cercaArticoli";
            this.tstb_cercaArticoli.Size = new System.Drawing.Size(100, 50);
            this.tstb_cercaArticoli.Text = "Cerca...";
            this.tstb_cercaArticoli.ToolTipText = "Cerca Articolo";
            this.tstb_cercaArticoli.Leave += new System.EventHandler(this.tstb_cercaArticoli_Leave);
            this.tstb_cercaArticoli.Click += new System.EventHandler(this.tstb_cercaArticoli_Click);
            this.tstb_cercaArticoli.TextChanged += new System.EventHandler(this.tstb_cercaArticoli_TextChanged);
            // 
            // toolStripSeparator7
            // 
            this.toolStripSeparator7.Name = "toolStripSeparator7";
            this.toolStripSeparator7.Size = new System.Drawing.Size(6, 50);
            // 
            // tsbtn_ExportArticoli
            // 
            this.tsbtn_ExportArticoli.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbtn_ExportArticoli.Image = ((System.Drawing.Image)(resources.GetObject("tsbtn_ExportArticoli.Image")));
            this.tsbtn_ExportArticoli.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbtn_ExportArticoli.Name = "tsbtn_ExportArticoli";
            this.tsbtn_ExportArticoli.Size = new System.Drawing.Size(51, 47);
            this.tsbtn_ExportArticoli.Text = "toolStripButton4";
            this.tsbtn_ExportArticoli.ToolTipText = "Esporta Articoli";
            this.tsbtn_ExportArticoli.Click += new System.EventHandler(this.tsbtn_ExportArticoli_Click);
            // 
            // tabClienti
            // 
            this.tabClienti.Controls.Add(this.dgv_Clienti);
            this.tabClienti.Controls.Add(this.toolStrip_Clienti);
            this.tabClienti.ImageIndex = 1;
            this.tabClienti.Location = new System.Drawing.Point(4, 60);
            this.tabClienti.Name = "tabClienti";
            this.tabClienti.Padding = new System.Windows.Forms.Padding(3);
            this.tabClienti.Size = new System.Drawing.Size(1234, 448);
            this.tabClienti.TabIndex = 2;
            this.tabClienti.ToolTipText = "Clienti";
            this.tabClienti.UseVisualStyleBackColor = true;
            // 
            // dgv_Clienti
            // 
            this.dgv_Clienti.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_Clienti.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgv_Clienti.Location = new System.Drawing.Point(3, 53);
            this.dgv_Clienti.Name = "dgv_Clienti";
            this.dgv_Clienti.ReadOnly = true;
            this.dgv_Clienti.Size = new System.Drawing.Size(1228, 392);
            this.dgv_Clienti.TabIndex = 5;
            this.dgv_Clienti.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_Clienti_CellClick);
            // 
            // toolStrip_Clienti
            // 
            this.toolStrip_Clienti.AutoSize = false;
            this.toolStrip_Clienti.BackColor = System.Drawing.SystemColors.Control;
            this.toolStrip_Clienti.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip_Clienti.ImageScalingSize = new System.Drawing.Size(47, 47);
            this.toolStrip_Clienti.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsbtn_AddCliente,
            this.toolStripSeparator6,
            this.tsbtn_RemoveCliente,
            this.toolStripSeparator2,
            this.tstb_cercaClienti,
            this.toolStripSeparator5,
            this.tsbtn_ExportClienti});
            this.toolStrip_Clienti.Location = new System.Drawing.Point(3, 3);
            this.toolStrip_Clienti.Name = "toolStrip_Clienti";
            this.toolStrip_Clienti.Size = new System.Drawing.Size(1228, 50);
            this.toolStrip_Clienti.TabIndex = 4;
            this.toolStrip_Clienti.Text = "toolStrip2";
            // 
            // tsbtn_AddCliente
            // 
            this.tsbtn_AddCliente.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.tsbtn_AddCliente.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbtn_AddCliente.Image = ((System.Drawing.Image)(resources.GetObject("tsbtn_AddCliente.Image")));
            this.tsbtn_AddCliente.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbtn_AddCliente.Name = "tsbtn_AddCliente";
            this.tsbtn_AddCliente.Size = new System.Drawing.Size(51, 47);
            this.tsbtn_AddCliente.ToolTipText = "Aggiungi Cliente";
            this.tsbtn_AddCliente.Click += new System.EventHandler(this.tsbtn_AddCliente_Click);
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            this.toolStripSeparator6.Size = new System.Drawing.Size(6, 50);
            // 
            // tsbtn_RemoveCliente
            // 
            this.tsbtn_RemoveCliente.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbtn_RemoveCliente.Image = ((System.Drawing.Image)(resources.GetObject("tsbtn_RemoveCliente.Image")));
            this.tsbtn_RemoveCliente.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbtn_RemoveCliente.Name = "tsbtn_RemoveCliente";
            this.tsbtn_RemoveCliente.Size = new System.Drawing.Size(51, 47);
            this.tsbtn_RemoveCliente.ToolTipText = "Rimuovi Cliente";
            this.tsbtn_RemoveCliente.Click += new System.EventHandler(this.tsbtn_RemoveCliente_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 50);
            // 
            // tstb_cercaClienti
            // 
            this.tstb_cercaClienti.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.tstb_cercaClienti.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.tstb_cercaClienti.Name = "tstb_cercaClienti";
            this.tstb_cercaClienti.Size = new System.Drawing.Size(100, 50);
            this.tstb_cercaClienti.Text = "Cerca...";
            this.tstb_cercaClienti.ToolTipText = "Cerca Cliente";
            this.tstb_cercaClienti.Leave += new System.EventHandler(this.tstb_cercaClienti_Leave);
            this.tstb_cercaClienti.Click += new System.EventHandler(this.tstb_cercaClienti_Click);
            this.tstb_cercaClienti.TextChanged += new System.EventHandler(this.tstb_cercaClienti_TextChanged);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(6, 50);
            // 
            // tsbtn_ExportClienti
            // 
            this.tsbtn_ExportClienti.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbtn_ExportClienti.Image = ((System.Drawing.Image)(resources.GetObject("tsbtn_ExportClienti.Image")));
            this.tsbtn_ExportClienti.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbtn_ExportClienti.Name = "tsbtn_ExportClienti";
            this.tsbtn_ExportClienti.Size = new System.Drawing.Size(51, 47);
            this.tsbtn_ExportClienti.ToolTipText = "Esporta Lista Clienti";
            this.tsbtn_ExportClienti.Click += new System.EventHandler(this.tsbtn_ExportClienti_Click);
            // 
            // Form_Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1242, 512);
            this.Controls.Add(this.tabControl);
            this.Name = "Form_Main";
            this.Text = "Service Management";
            this.tabControl.ResumeLayout(false);
            this.tabRiparazioni.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_Riparazioni)).EndInit();
            this.toolStrip_Riparazioni.ResumeLayout(false);
            this.toolStrip_Riparazioni.PerformLayout();
            this.tabArticoli.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_Articoli)).EndInit();
            this.toolStrip_Prodotti.ResumeLayout(false);
            this.toolStrip_Prodotti.PerformLayout();
            this.tabClienti.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_Clienti)).EndInit();
            this.toolStrip_Clienti.ResumeLayout(false);
            this.toolStrip_Clienti.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ImageList imageList;
        private System.Windows.Forms.TabControl tabControl;
        private System.Windows.Forms.TabPage tabRiparazioni;
        private System.Windows.Forms.TabPage tabArticoli;
        private System.Windows.Forms.ToolStrip toolStrip_Prodotti;
        private System.Windows.Forms.ToolStripButton tsbtn_AddArticolo;
        private System.Windows.Forms.ToolStripButton tsbtn_RemoveArticolo;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripButton tsbtn_ExportArticoli;
        private System.Windows.Forms.ToolStrip toolStrip_Riparazioni;
        private System.Windows.Forms.ToolStripButton tsbtn_AddRiparazione;
        private System.Windows.Forms.ToolStripButton tsbtn_RemoveRiparazione;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton tsbtn_ExportRiparazioni;
        private System.Windows.Forms.TabPage tabClienti;
        private System.Windows.Forms.DataGridView dgv_Clienti;
        private System.Windows.Forms.ToolStrip toolStrip_Clienti;
        private System.Windows.Forms.ToolStripButton tsbtn_AddCliente;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
        private System.Windows.Forms.ToolStripButton tsbtn_RemoveCliente;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripTextBox tstb_cercaClienti;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripButton tsbtn_ExportClienti;
        private System.Windows.Forms.DataGridView dgv_Articoli;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripTextBox tstb_cercaArticoli;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator7;
        private System.Windows.Forms.ToolStripButton tsbtn_PrintRiparazione;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator8;
        private System.Windows.Forms.ToolStripTextBox tstb_cercaRiparazioni;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator9;
        private System.Windows.Forms.DataGridView dgv_Riparazioni;
    }
}

