﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Service_Management
{
    public partial class Form_Cliente : Form
    {
        private int indexRecordToEdit = -1;
        public Form_Cliente()
        {
            try
            {
                InitializeComponent();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// Overload del costruttore con 0 parametri, richiamato tramite :this()
        /// </summary>
        /// <param name="indexRecordToEdit"></param>
        public Form_Cliente(int indexRecordToEdit) : this()
        {
            this.indexRecordToEdit = indexRecordToEdit;
            fillForm();
        }

        private void fillForm()
        {
            DataTable dt = Utility.getDataTableFromDB("SELECT * FROM [Clienti] WHERE [Id] = '" + indexRecordToEdit.ToString() + "'");
            if (!dt.Rows.Count.Equals(0))
            {
                tb_Nome.Text = dt.Rows[0].Field<string>("Nome");
                tb_PIVA.Text = dt.Rows[0].Field<string>("P_IVA");
                tb_CodiceFiscale.Text = dt.Rows[0].Field<string>("Codice_Fiscale");
                tb_Indirizzo.Text = dt.Rows[0].Field<string>("Indirizzo");
                tb_Provincia.Text = dt.Rows[0].Field<string>("Provincia");
                tb_Paese.Text = dt.Rows[0].Field<string>("Paese");
                tb_CAP.Text = dt.Rows[0].Field<string>("CAP");
                tb_Stato.Text = dt.Rows[0].Field<string>("Stato");
                tb_Telefono.Text = dt.Rows[0].Field<string>("Telefono");
                tb_SitoInternet.Text = dt.Rows[0].Field<string>("Sito_Internet");
                tb_Fax.Text = dt.Rows[0].Field<string>("Fax");
                tb_eMail.Text = dt.Rows[0].Field<string>("eMail");
            }
        }

        private void btn_OK_Click(object sender, EventArgs e)
        {
            try
            {
                if (tb_Nome.Text.Equals(""))
                    MessageBox.Show("Completare i campi obbligatori prima di procedere", "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
                else
                {
                    if (indexRecordToEdit.Equals(-1))
                        searchRecordInDB();
                    else
                        updateRecordInDB();
                    if (this.DialogResult.Equals(DialogResult.OK))
                        this.Dispose();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void searchRecordInDB()
        {
            try
            {
                DataTable dt = Utility.getDataTableFromDB("SELECT * FROM [Clienti] WHERE " +
                       "[Nome] = '" + tb_Nome.Text + "' AND " +
                       "[P_IVA] = '" + tb_PIVA.Text + "' AND " +
                       "[Codice_Fiscale] = '" + tb_CodiceFiscale.Text + "'");
                if (!dt.Rows.Count.Equals(0))
                {
                    DialogResult result = MessageBox.Show("Il record già presente nel Database verrà aggiornato con i dati immessi nel form.\nSei sicuro di voler procedere?", "Aggiornamento", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (result.Equals(DialogResult.Yes))
                    {
                        indexRecordToEdit = dt.Rows[0].Field<int>(0);
                        updateRecordInDB();
                        this.DialogResult = DialogResult.OK;
                    }
                    else
                        this.DialogResult = DialogResult.Abort;
                }
                else
                    insertRecordInDB();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void insertRecordInDB()
        {
            try
            {
                Utility.executeQuery("INSERT INTO [Clienti] " +
                "([Nome],[P_IVA],[Codice_Fiscale],[Indirizzo],[Provincia],[Paese],[CAP],[Stato],[Telefono],[Sito_Internet],[Fax],[eMail]) " +
                "VALUES ('" + tb_Nome.Text + "'," +
                    "'" + tb_PIVA.Text + "'," +
                    "'" + tb_CodiceFiscale.Text + "'," +
                    "'" + tb_Indirizzo.Text + "'," +
                    "'" + tb_Provincia.Text + "'," +
                    "'" + tb_Paese.Text + "'," +
                    "'" + tb_CAP.Text + "'," +
                    "'" + tb_Stato.Text + "'," +
                    "'" + tb_Telefono.Text + "'," +
                    "'" + tb_SitoInternet.Text + "'," +
                    "'" + tb_Fax.Text + "'," +
                    "'" + tb_eMail.Text + "')");
                this.DialogResult = DialogResult.OK;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void updateRecordInDB()
        {
            try
            {
                Utility.executeQuery("UPDATE [Clienti] SET " +
                "[Nome] = '" + tb_Nome.Text + "'," +
                "[P_IVA] = '" + tb_PIVA.Text + "'," +
                "[Codice_Fiscale] = '" + tb_CodiceFiscale.Text + "'," +
                "[Indirizzo] = '" + tb_Indirizzo.Text + "'," +
                "[Provincia] = '" + tb_Provincia.Text + "'," +
                "[Paese] = '" + tb_Paese.Text + "'," +
                "[CAP] = '" + tb_CAP.Text + "'," +
                "[Stato] = '" + tb_Stato.Text + "'," +
                "[Telefono] = '" + tb_Telefono.Text + "'," +
                "[Sito_Internet] = '" + tb_SitoInternet.Text + "'," +
                "[Fax] = '" + tb_Fax.Text + "'," +
                "[eMail] = '" + tb_eMail.Text + "'" +
                "WHERE [Id] = '" + indexRecordToEdit.ToString() + "'");
                this.DialogResult = DialogResult.OK;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_Cancel_Click(object sender, EventArgs e)
        {
            try
            {
                this.DialogResult = DialogResult.Cancel;
                this.Dispose();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void tb_Nome_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (tb_Nome.Text.Equals(""))
                    tb_Nome.BackColor = Color.Salmon;
                else
                    tb_Nome.BackColor = Color.White;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
