﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Resources;
using System.Data.SqlClient;

namespace Service_Management
{
    public partial class Form_Main : Form
    {
        private string selectCondition = "";
        public Form_Main()
        {
            try
            {
                InitializeComponent();
                dgv_Clienti.DataSource = Utility.aggiornaDataGridView("SELECT * FROM [Clienti]");
                dgv_Articoli.DataSource = Utility.aggiornaDataGridView("SELECT * FROM [Articoli]");
                dgv_Riparazioni.DataSource = Utility.aggiornaDataGridView("SELECT * FROM [Vista_Riparazioni]");
                //dgv_Riparazioni.Columns["Id_Cliente"].Visible = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        #region MetodiGenerali

        /// <summary>
        /// Ripristina il valore di default della textbox di ricerca e tutte le datagridview
        /// </summary>
        /// <param name="tb"></param>
        private void resetSearchTextboxAndDatagridviews(ToolStripTextBox tb)
        {
            if (tstb_cercaClienti.Text.Equals(""))
            {
                tstb_cercaClienti.Text = "Cerca...";
                dgv_Clienti.DataSource = Utility.aggiornaDataGridView("SELECT * FROM [Clienti]");
            }
            if (tstb_cercaArticoli.Text.Equals(""))
            {
                tstb_cercaArticoli.Text = "Cerca...";
                dgv_Articoli.DataSource = Utility.aggiornaDataGridView("SELECT * FROM [Articoli]");
            }
            if (tstb_cercaRiparazioni.Text.Equals(""))
            {
                tstb_cercaRiparazioni.Text = "Cerca...";
                dgv_Riparazioni.DataSource = Utility.aggiornaDataGridView("SELECT * FROM [Vista_Riparazioni]");
            }
            //aggiungere le atre ricerche QUI...
        }

        /// <summary>
        /// modifica la visualizzazione dei dati nella datagridview in accordo con la selezione scritta nella corrispondente textbox di ricerca
        /// </summary>
        /// <param name="tb"></param>
        private void changeDataDisplayed(ToolStripTextBox tb)
        {
            try
            {
                if (!tstb_cercaClienti.Text.Equals("") && !tstb_cercaClienti.Text.Equals("Cerca..."))
                {
                    selectCondition = Utility.getSQLCondition_ForeachColumn("Clienti", tstb_cercaClienti.Text);
                    dgv_Clienti.DataSource = Utility.aggiornaDataGridView("SELECT * FROM [Clienti]" + selectCondition);
                }
                if (!tstb_cercaArticoli.Text.Equals("") && !tstb_cercaArticoli.Text.Equals("Cerca..."))
                {
                    selectCondition = Utility.getSQLCondition_ForeachColumn("Articoli", tstb_cercaArticoli.Text);
                    dgv_Articoli.DataSource = Utility.aggiornaDataGridView("SELECT * FROM [Articoli]" + selectCondition);
                }
                if (!tstb_cercaRiparazioni.Text.Equals("") && !tstb_cercaRiparazioni.Text.Equals("Cerca..."))
                {
                    selectCondition = Utility.getSQLCondition_ForeachColumn("Riparazioni", tstb_cercaRiparazioni.Text);
                    dgv_Riparazioni.DataSource = Utility.aggiornaDataGridView("SELECT * FROM [Vista_Riparazioni]" + selectCondition);
                }
                //aggiungere le atre ricerche QUI...
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        #endregion

        #region Riparazioni

        private void tstb_cercaRiparazioni_Click(object sender, EventArgs e)
        {
            tstb_cercaRiparazioni.Text = "";
            tstb_cercaRiparazioni_TextChanged(null, null);
        }

        private void tstb_cercaRiparazioni_Leave(object sender, EventArgs e)
        {
            resetSearchTextboxAndDatagridviews(tstb_cercaRiparazioni);
        }

        private void tstb_cercaRiparazioni_TextChanged(object sender, EventArgs e)
        {
            changeDataDisplayed(tstb_cercaRiparazioni);
        }

        private void tsbtn_AddRiparazione_Click(object sender, EventArgs e)
        {
            try
            {
                Form_Riparazione formRip = new Form_Riparazione();
                var dialogResult = formRip.ShowDialog();
                if (dialogResult.Equals(DialogResult.OK))
                    dgv_Riparazioni.DataSource = Utility.aggiornaDataGridView("SELECT * FROM [Vista_Riparazioni]");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void tsbtn_RemoveRiparazione_Click(object sender, EventArgs e)
        {
            try
            {
                DialogResult dialogResult = MessageBox.Show("L'elemento selezionato verrà rimosso dal database.\nSei sicuro di voler procedere?", "Elimina Riparazione", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (dialogResult == DialogResult.Yes)
                {
                    int actualIndex = dgv_Riparazioni.CurrentCell.RowIndex;
                    Utility.executeQuery("DELETE FROM [Riparazioni] WHERE " +
                           "[Id] = '" + dgv_Riparazioni.Rows[actualIndex].Cells["Id"].Value.ToString() + "'");
                    tstb_cercaRiparazioni.Text = "";
                    dgv_Riparazioni.DataSource = Utility.aggiornaDataGridView("SELECT * FROM [Vista_Riparazioni]");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void tsbtn_ExportRiparazioni_Click(object sender, EventArgs e)
        {
            try
            {
                Utility.saveAndVisualizeExcelReport(dgv_Riparazioni, "Report Riparazioni");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void tsbtn_PrintRiparazione_Click(object sender, EventArgs e)
        {
            try
            {
                Utility.saveAndVisualizeWordReport(dgv_Riparazioni, dgv_Riparazioni.CurrentCell.RowIndex);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        #endregion

        #region Articoli

        private void tstb_cercaArticoli_Click(object sender, EventArgs e)
        {
            tstb_cercaArticoli.Text = "";
            tstb_cercaArticoli_TextChanged(null, null);
        }

        private void tstb_cercaArticoli_Leave(object sender, EventArgs e)
        {
            resetSearchTextboxAndDatagridviews(tstb_cercaArticoli);
        }

        private void tstb_cercaArticoli_TextChanged(object sender, EventArgs e)
        {
            changeDataDisplayed(tstb_cercaArticoli);
        }

        private void tsbtn_AddArticolo_Click(object sender, EventArgs e)
        {
            try
            {
                Form_Articolo formArt = new Form_Articolo();
                var dialogResult = formArt.ShowDialog();
                if (dialogResult.Equals(DialogResult.OK))
                    dgv_Articoli.DataSource = Utility.aggiornaDataGridView("SELECT * FROM [Articoli]");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dgv_Articoli_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (dgv_Articoli.Rows[dgv_Articoli.CurrentCell.RowIndex].Cells["Id"].Value.ToString() != "" && !dgv_Articoli.SelectedCells.Count.Equals(dgv_Articoli.Rows[dgv_Articoli.CurrentCell.RowIndex].Cells.Count))
                {
                    int indexRecordToEdit = Convert.ToInt32(dgv_Articoli.Rows[dgv_Articoli.CurrentCell.RowIndex].Cells["Id"].Value.ToString());
                    Form_Articolo formArt = new Form_Articolo(indexRecordToEdit);
                    var dialogResult = formArt.ShowDialog();
                    if (dialogResult.Equals(DialogResult.OK))
                        dgv_Articoli.DataSource = Utility.aggiornaDataGridView("SELECT * FROM [Articoli]");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void tsbtn_RemoveArticolo_Click(object sender, EventArgs e)
        {
            try
            {
                DialogResult dialogResult = MessageBox.Show("L'elemento selezionato verrà rimosso dal database.\nSei sicuro di voler procedere?", "Elimina Articolo", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (dialogResult == DialogResult.Yes)
                {
                    int actualIndex = dgv_Articoli.CurrentCell.RowIndex;
                    Utility.executeQuery("DELETE FROM [Articoli] WHERE " +
                           "[Id] = '" + dgv_Articoli.Rows[actualIndex].Cells["Id"].Value.ToString() + "' AND " +
                           "[Descrizione] = '" + dgv_Articoli.Rows[actualIndex].Cells["Descrizione"].Value.ToString() + "' AND " +
                           "[Prezzo] = '" + dgv_Articoli.Rows[actualIndex].Cells["Prezzo"].Value.ToString().PadLeft(8, '0').Replace(',','.') + "'");
                    tstb_cercaArticoli.Text = "";
                    dgv_Articoli.DataSource = Utility.aggiornaDataGridView("SELECT * FROM [Articoli]");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void tsbtn_ExportArticoli_Click(object sender, EventArgs e)
        {
            try
            {
                Utility.saveAndVisualizeExcelReport(dgv_Articoli, "Report Articoli");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        #endregion

        #region Clienti

        private void tstb_cercaClienti_Click(object sender, EventArgs e)
        {
            tstb_cercaClienti.Text = "";
            tstb_cercaClienti_TextChanged(null, null);
        }

        private void tstb_cercaClienti_Leave(object sender, EventArgs e)
        {
            resetSearchTextboxAndDatagridviews(tstb_cercaClienti);
        }

        private void tstb_cercaClienti_TextChanged(object sender, EventArgs e)
        {
            changeDataDisplayed(tstb_cercaClienti);
        }

        private void tsbtn_AddCliente_Click(object sender, EventArgs e)
        {
            try
            {
                Form_Cliente formCli = new Form_Cliente();
                var dialogResult = formCli.ShowDialog();
                if (dialogResult.Equals(DialogResult.OK))
                    dgv_Clienti.DataSource = Utility.aggiornaDataGridView("SELECT * FROM [Clienti]");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dgv_Clienti_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (dgv_Clienti.Rows[dgv_Clienti.CurrentCell.RowIndex].Cells["Id"].Value.ToString() != "" && !dgv_Clienti.SelectedCells.Count.Equals(dgv_Clienti.Rows[dgv_Clienti.CurrentCell.RowIndex].Cells.Count))
                {
                    int indexRecordToEdit = Convert.ToInt32(dgv_Clienti.Rows[dgv_Clienti.CurrentCell.RowIndex].Cells["Id"].Value.ToString());
                    Form_Cliente formCli = new Form_Cliente(indexRecordToEdit);
                    var dialogResult = formCli.ShowDialog();
                    if (dialogResult.Equals(DialogResult.OK))
                        dgv_Clienti.DataSource = Utility.aggiornaDataGridView("SELECT * FROM [Clienti]" + selectCondition);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }


        private void tsbtn_RemoveCliente_Click(object sender, EventArgs e)
        {
            try
            {
                DialogResult dialogResult = MessageBox.Show("L'elemento selezionato verrà rimosso dal database.\nSei sicuro di voler procedere?", "Elimina Cliente", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (dialogResult == DialogResult.Yes)
                {
                    int actualIndex = dgv_Clienti.CurrentCell.RowIndex;
                    Utility.executeQuery("DELETE FROM [Clienti] WHERE " +
                           "[Id] = '" + dgv_Clienti.Rows[actualIndex].Cells["Id"].Value.ToString() + "' AND " +
                           "[Nome] = '" + dgv_Clienti.Rows[actualIndex].Cells["Nome"].Value.ToString() + "' AND " + 
                           "[P_IVA] = '" + dgv_Clienti.Rows[actualIndex].Cells["P_IVA"].Value.ToString() + "' AND " +
                           "[Codice_Fiscale] = '" + dgv_Clienti.Rows[actualIndex].Cells["Codice_Fiscale"].Value.ToString() + "' AND " +
                           "[Indirizzo] = '" + dgv_Clienti.Rows[actualIndex].Cells["Indirizzo"].Value.ToString() + "' AND " +
                           "[Provincia] = '" + dgv_Clienti.Rows[actualIndex].Cells["Provincia"].Value.ToString() + "' AND " +
                           "[Paese] = '" + dgv_Clienti.Rows[actualIndex].Cells["Paese"].Value.ToString() + "' AND " +
                           "[CAP] = '" + dgv_Clienti.Rows[actualIndex].Cells["CAP"].Value.ToString() + "' AND " +
                           "[Stato] = '" + dgv_Clienti.Rows[actualIndex].Cells["Stato"].Value.ToString() + "' AND " +
                           "[Telefono] = '" + dgv_Clienti.Rows[actualIndex].Cells["Telefono"].Value.ToString() + "' AND " +
                           "[Sito_Internet] = '" + dgv_Clienti.Rows[actualIndex].Cells["Sito_Internet"].Value.ToString() + "' AND " +
                           "[Fax] = '" + dgv_Clienti.Rows[actualIndex].Cells["Fax"].Value.ToString() + "' AND " +
                           "[eMail] = '" + dgv_Clienti.Rows[actualIndex].Cells["eMail"].Value.ToString() + "'");
                    tstb_cercaClienti.Text = "";
                    dgv_Clienti.DataSource = Utility.aggiornaDataGridView("SELECT * FROM [Clienti]");
                }                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void tsbtn_ExportClienti_Click(object sender, EventArgs e)
        {
            try
            {
                Utility.saveAndVisualizeExcelReport(dgv_Clienti, "Report Clienti");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }


        #endregion
    }
}
